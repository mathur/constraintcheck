import prog_representation
import numpy as np
from sklearn import svm

def build(rep):
    X = np.array(rep.vars_all)
    Y = np.array(rep.res_all)
    print("X:", X)
    print("Y:", Y)
    clf = svm.SVC(cache_size=2500, kernel='linear')
    clf.fit(X, Y)
    print(clf.coef_)
    print(clf.intercept_)
    print(clf.predict([[10, 10, 10, 5]]))
    print(clf.predict([[10, 10, 10,  10]]))
    print(clf.predict([[10, 10, 10, 20]]))
    print(clf.predict([[10, 10, 10, 25]]))
    print(clf.predict([[10, 10, 10, 30]]))
    print(clf.predict([[10, 10, 10, 200]]))
