import copy
import operator
import random
import math
import itertools
# SVM tools
import numpy as np
from sklearn import svm
import copy
# To make sort + sub restrictions work
import pickle

# Sampling vars (relevant to stateful sampling)
move_up_score = 8
move_below_score = -3
done_score = 16
remove_score = -12

# Constants for sampling of parameters.
low_val = 1
inf = 100

# Some specific multipliers to get better constraint limits
muls = [1 / 2.0, 1 / math.sqrt(2.0), 1.0, math.sqrt(2.0), 2.0]
# The left and right shoulders of the muls list
mul_shoulder_l = [i - (i * 0.1) for i in muls]
mul_shoulder_r = [i + (i * 0.1) for i in muls]
# Set by prog_rep's init, tells whether there are additional predicates and analysis on sorted lists
sort_mode_active = False
# The min samples required to move a restr from the sort structure to must_restr. Set via command line (via prog_rep)
sort_min_samples = 20  # TODO: Also used in the SVM technique
# The maximum number of steps to go back while checking for sorted predicates
sort_max_steps = 3
# The maximum number of samples in the sort mode, at which point, the key can be removed
sort_max_samples = 6000

# Add extended predicates or not (set manually)
enable_extended_predicates = True

# Use an upper limit of predicates that are tested at one time.
pred_upper_limit = 50
# Use differences to produce more predicates?
pred_use_diff = True


# Specialist args collected so far
args_create = []  # args involved in the create
args_alt = []
args_offset = []  # Basically, args involved in 2D operations
args_cut = []  # args in 2D operations that are cutting

class restriction:
    """
    A representation of the restriction applied,
    GEQ
    GT
    LT
    LEQ
    UNB: Unbounded maximum

    Regression technique:
    if src < dest:
        dest - src = c (c>0)
    if src > dest:
        des - src = c (c<0)
    We try to find samples with c +ve, -ve and try to get close to 0.

    Sort based technique:
    Instantiate several predicates (thereby ensuring that at least one of them provides results independent of other predicates),
    then use sorted values to find the demarcation between pass/fail.
    """

    def __init__(self, src, rel, dest=None, src_val=None, dest_val=None, weak=False, score=1, success=True,
                 is_extended=False, extended_dest=None, extended_mode=""):
        """
        Makes an object of type restriction.
        Restrictions can be LT, GT, UNB, etc.
        An arg/val may be provided for the restriction.
        :param rel: The kind of relationship
        :param dest: param occuring on the RHS
        :param extended_dest: a list of params occuring on the RHS
        """
        self.src = src
        self.rel = rel
        if dest is not None:
            self.dest = dest
        else:
            self.dest = ""
        self.pve_score = score
        self.neg_score = 0
        # Weak restrictions do not exist by themselves, but only as counterexamples to a non-weak restriction.
        self.weak = weak
        # Is the restriction created from a successful or an unsuccessful operation?
        # This is used in the sorted mode to figure out whether the operation was a success, or failure.
        self.success = success
        self.src_val = src_val
        if not rel == "UNB":
            self.dest_val = dest_val
            # Set the constant 'c' that quantifies the difference
            if self.dest_val is not None: self.c = self.src_val - self.dest_val
            # The multiplier applied to the dest
        self.mul = 1.0
        # store a backup if this exists
        self.backup = None
        # Flag if this is an extended restriction
        self.is_extended = is_extended
        # The dest in extended mode
        self.extended_dest = extended_dest
        # The mode used for complex predicate (e.g. min, max, etc.)
        if self.extended_dest is not None:
            self.extended_len = len(pickle.loads(self.extended_dest))
        else:
            self.extended_len = None
        self.extended_mode = extended_mode

    def inc_score(self):
        self.pve_score = self.pve_score + 1
        return self

    def dec_score(self):
        self.neg_score = self.neg_score - 1
        return self

    def reset_score(self):
        self.pve_score = 0
        self.neg_score = 0

    def next_mul(self):
        # Make a copy of self to be used as backup
        restr_old = copy.copy(self)
        # Move to the next multiplier
        index = muls.index(self.mul)
        if len(muls) > index + 1:
            # TODO: Currently only if the set of known multipliers can
            self.mul = muls[index + 1]
            self.reset_score()
            self.backup = restr_old
        # else:
        #    self = None
        return self

    def prev_mul(self):
        # Make a copy of self to be used as backup
        restr_old = copy.copy(self)
        # Move to the previous multiplier
        index = muls.index(self.mul)
        if index > 0:
            # TODO: Currently only if the set of known multipliers can
            self.mul = muls[index - 1]
            self.reset_score()
            self.backup = restr_old
        # else:
        #    self = None
        return self

    def compare(self, r):
        """
        Compares the passed restriction to self.
        :param r: Another restriction for comparison
        :return: 1-> same, 0-> different dest, -1-> opposite restr
        """
        # Assert that both src are same
        assert self.src == r.src
        # If the destination is the same
        if self.dest == r.dest:
            if self.rel == r.rel:
                return 1
            else:
                return -1
        else:
            return 0

    def get_sample(self, vars_curr):
        """
        Sample using this restriction.
        When sampling the whole list, start from restrictions on smaller indices, and go up.
        :param vars_curr: The list of vars used currently.
        :return:
        """
        # Don't do anything if unbounded restriction
        if self.rel == "UNB":
            return vars_curr

        if self.extended_mode not in ["min_2c", "min_sub"]:
            # Can be simply evaluated
            val = self.mul * self.eval_val(vars_curr)
        else:
            temp = self.eval_val(vars_curr)
            temp_m = []
            if self.extended_mode == "min_2c":
                for i in range(0, len(self.mul)):
                    temp_m.append(self.mul[i] * temp[i])
                val = min(temp_m)

            elif self.extended_mode == "min_sub":
                # Counter for number of sub in the min, useful for indexing muls
                i = 0
                for e in temp:
                    # See if a sub is required, or if it is not
                    if type(e) == list:
                        # Add the evaluated sub # TODO: mul needs to generalize
                        temp_m.append(e[0] - self.mul[i] * e[1])
                        i = i + 1
                    else:
                        temp_m.append(e)
                    val = min(temp_m)

        if self.rel == "LT":
            if vars_curr[int(self.src)] >= val:
                vars_curr[int(self.src)] = random.uniform(low_val, val)
        elif self.rel == "GT":
            if vars_curr[int(self.src)] <= val:
                vars_curr[int(self.src)] = random.uniform(val, inf)

        # Do nothing if self.rel == "UNB"
        return vars_curr

    def get_sample_near(self, vars_curr, near=0.05):
        """
        Used in the LP mode to fine tune multipliers (sample around the boundary condition).
        Sample with this restriction.
        When sampling the whole list, start from restrictions on smaller indices, and go up.
        :param vars_curr: The list of vars used currently.
        :param near: sampling is done by stretching the multiplier * near (Default=5%)
        :return:
        """

        if self.extended_mode not in ["min_2c", "min_sub"]:
            # Can be simply evaluated
            val = self.eval_val(vars_curr)
            if self.rel == "LT":
                mul_mod = self.mul + near
                vars_curr[int(self.src)] = random.uniform(self.mul * val, mul_mod * val)
            elif self.rel == "GT":
                mul_mod = max(self.mul - near, 0.01)
                vars_curr[int(self.src)] = random.uniform(mul_mod * val, self.mul * val)

        else:
            # get each element involved in the min
            elems = self.eval_val(vars_curr)
            # evaluate with self.mul, and slightly above, and below
            temp_init = []
            temp_l = []
            temp_h = []
            if self.extended_mode == "min_2c":
                for i in range(0, len(self.mul)):
                    temp_init.append(self.mul[i] * elems[i])
                    temp_l.append((self.mul[i] - near) * elems[i])
                    temp_h.append((self.mul[i] + near) * elems[i])

                if self.rel == "LT":
                    vars_curr[int(self.src)] = random.uniform(min(temp_init), min(temp_h))
                elif self.rel == "GT":
                    vars_curr[int(self.src)] = random.uniform(min(temp_l), min(temp_init))

            elif self.extended_mode == "min_sub":
                elems = self.eval_val(vars_curr)
                # A temporary list that collects everything
                temp = []
                for e in elems:
                    # See if a sub is required, or if it is not
                    if type(e) == list:
                        # Add the evaluated sub
                        temp.append(e[0] - e[1])
                    else:
                        temp.append(e)
                val = min(temp)

                # Set the requisite mul_mod. For some reason,  self.mul is a list here
                if self.rel == "LT":
                    mul_mod = self.mul[0] + near
                    vars_curr[int(self.src)] = random.uniform(self.mul[0] * val, mul_mod * val)
                elif self.rel == "GT":
                    # don't want something negative
                    mul_mod = max(self.mul[0] - near, 0.01)
                    vars_curr[int(self.src)] = random.uniform(mul_mod * val, self.mul[0] * val)

        # Ignore UNB restrictions

        return vars_curr

    def eval_tuple(self, tup, vars_curr):
        # The elems thus evaluated may be a list of strings or a list of subs, or a mixture.
        elem_vals = []
        # Now find concrete values of all the elements involved in the predicate
        for elem in tup:
            if type(elem) == str:
                elem_vals.append(vars_curr[int(elem)])
            if type(elem) == sub:
                # Note that it can be a standard sub, or used with min_sub type, where each element is important
                if self.extended_mode == "min_sub":
                    elem_vals.append(elem.tuple(vars_curr))
                else:
                    # Standard
                    elem_vals.append(elem.eval(vars_curr))
        if self.extended_mode == "min":
            # print("Evaluating extended", self, elem_vals)
            return min(elem_vals)
        elif self.extended_mode == "max":
            return max(elem_vals)
        elif self.extended_mode in ["min_2c", "min_sub"]:
            return tuple(elem_vals)
        else:
            print("Unidentified extension modifier!")

    def eval_val(self, vars_curr):
        """
        Evals the dest. part of the restriction
        """
        if self.is_extended:
            # If the restriction is based on the extended mode, then the predicate needs to be evaluated
            # We pickle the extended dest at the point of restriction creation.
            elems = pickle.loads(self.extended_dest)
            if type(elems) == tuple:
                # min_sub also goes here
                return self.eval_tuple(elems, vars_curr)
            elif type(elems) == sub:
                return elems.eval(vars_curr)
        else:
            return vars_curr[int(self.dest)]

    def holds(self, vars_curr):
        """
        Checks if this restr holds for the current list of vars
        :param vars_curr: the list of vars in use. The values are passed.
        Note that in the current class, src and dest are stored as indices.
        :return: True (holds) or False (doesn't)
        """
        if self.rel == "UNB":
            return True
        if self.rel == "LT":
            if vars_curr[int(self.src)] < self.mul * self.eval_val(vars_curr):
                return True
            else:
                return False

        if self.rel == "GT":
            if vars_curr[int(self.src)] > self.mul * self.eval_val(vars_curr):
                return True
            else:
                return False

    def eval_multiplier(self, sorted_samples):
        """
        For the mode with additional predicates + sorting, evaluate the multiplier.
        Note that this is not stored in the internal variable.
        :param sorted_samples: the samples corresponding to this restriction ({val_1:True, ...., val_n:False...},
        or the other way around)
        """
        lst_sample_vals = list(sorted_samples.values())
        lst_sample_keys = list(sorted_samples.keys())
        old_sample = lst_sample_vals[0]
        # TODO: Bug checking on the logic, the assumption that the multiple is the first / last succeeding sample is not correct
        if old_sample is True:
            # The pattern is [True, True..., False, False..]
            for i in range(1, len(lst_sample_vals)):
                if lst_sample_vals[i] is False:
                    # The relevant index is i-1 (last passing)
                    self.mul = lst_sample_keys[i - 1]
                    break
        else:
            for i in range(1, len(lst_sample_vals)):
                if lst_sample_vals[i] is True:
                    # The relevant index is i (first passing)
                    self.mul = lst_sample_keys[i]
                    break

        # self.mul holds the multiplier, make the multiplier more standard if it fits in the shoulders of
        # For each element in the muls list
        for i in range(0, len(muls)):
            # self.mul lies between the shoulders, set it to mul
            if mul_shoulder_l[i] < self.mul < mul_shoulder_r[i]:
                print("Modifying multiplier from", self.mul, "to", muls[i])
                self.mul = muls[i]
                break

        return self

    def __eq__(self, other):
        """
        Equality of two restriction objects
        :param other: The other object
        :return: True or False
        """
        if self.src == other.src and self.dest == other.dest and self.rel == other.rel and self.extended_mode == other.extended_mode:
            return True
        else:
            return False

    def __hash__(self):
        return hash(self.__repr__())

    def __repr__(self):
        """
        Pretty print the restriction representation
        """
        return str(self.src) + " " + self.rel + self.extended_mode + str(self.dest)

    def __str__(self):
        """
        Pretty print the restriction representation
        """
        return str(self.src) + " " + self.rel + " " + str(self.mul) + " * " + self.extended_mode + str(self.dest)


def update_sorted_predicates(sorted_struct, r, removed_restr):
    """
    Updates list of predicates being tried. Predicates are updated / removed.
    :param :sorted_struct: The current structure of things being tried
    """
    if r not in removed_restr:
        if r in sorted_struct:
            print("----->Adding existing", r, "with evaluation", r.dest_val, "and result", r.success)
            # If entry for r exists in sorted_struct
            sorted_struct[r][r.dest_val] = r.success
            # Sort the dictionary at r according to dest_val
            sorted_struct[r] = dict(sorted(sorted_struct[r].items(), key=operator.itemgetter(0)))

            # Delete r if there is more than one transition
            # Find the number of transitions by going over all the values
            trans = 1
            temp_val = None
            for val in sorted_struct[r].values():
                if temp_val is None:
                    temp_val = val
                elif temp_val != val:
                    trans = trans + 1
                    temp_val = val
                else:
                    continue
                if trans >= 3:
                    # If there are more than three transitions, this restr should be removed
                    print("----->Removing", r, "with entries", sorted_struct[r],
                          "due to many transitions from pass-fail (or vice versa).")
                    removed_restr.append(r)
                    del sorted_struct[r]
                    # Stop the loop as it is not useful
                    break
                elif len(sorted_struct[r]) >= sort_max_samples:
                    # If there are more samples than a max, this should be removed
                    print("----->Removing", r, "with entries", sorted_struct[r], "due to max samples being reached.")
                    removed_restr.append(r)
                    del sorted_struct[r]
                    # Stop the loop as it is not useful
                    break
        else:
            print("----->Adding new", r, "with evaluation", r.dest_val)
            # Note: Check for weak predicate removed
            # Only add the entry if it is not weak
            sorted_struct[r] = {r.dest_val: r.success}
    else:
        print("----->NOT adding on", r)
    return sorted_struct, removed_restr


def add_rstr_to_must(rstr, must):
    # Add the current restricton to the must restriction collection (type {src1->[restr1, restr2...]; src2->..})
    if rstr.src in must:
        must[rstr.src].append(rstr)
    else:
        must[rstr.src] = [rstr]

    return must


def update_sorted_must(sorted_struct, removed_restr, must_restr):
    """
    Move relevant retrictions from the sorted struct to must restrictions.

    must_restr := {src1->[restr1, restr2...]; src2->..}
    """
    # Extract the keys of sorted_struct (type restriction)
    sorted_keys = list(sorted_struct.keys())
    # Keys that will be transferred to must restr
    useful_keys = []
    # If there are many keys, extract those that just occur once
    if len(sorted_keys) > 1:
        curr_key = sorted_keys[0]
        count = 1
        for i in range(1, len(sorted_keys)):
            if sorted_keys[i].src == curr_key.src:
                count = count + 1
                continue
            else:
                if count == 1:
                    # The previous index has the useful key
                    useful_keys.append(sorted_keys[i - 1])
                # Set the currently used key to be
                curr_key = sorted_keys[i]
                # Reset counter
                count = 1
    # If there is just one key, the choice is simple
    if len(sorted_keys) == 1:
        useful_keys.append(sorted_keys[0])

    # Move the useful keys to must restrictions
    for k in useful_keys:
        # Before moving the restrictions, check if they have atleast the min number of samples.
        # Count the number of successes and failures on the given constraint
        count_success = 0
        for val in sorted_struct[k].values():
            if val is True:
                count_success = count_success + 1
        # Make sure that succeses and failures are balanced, and we have enough samples
        if (len(sorted_struct[k]) >= sort_min_samples) and (count_success >= sort_min_samples / 2) and (
                len(sorted_struct[k]) - count_success >= sort_min_samples / 2):
            # k is of type restr
            # Make a deep copy and evaluate the multiplier required
            print("Upgrading to must", k, "with samples", sorted_struct[k])
            temp = copy.deepcopy(k)
            temp = temp.eval_multiplier(sorted_struct[temp])
            if k.src in must_restr.keys():
                must_restr[k.src].append(temp)
            else:
                must_restr[k.src] = [temp]
            removed_restr.append(k)
            del sorted_struct[k]
    return sorted_struct, removed_restr, must_restr


def update_restrictions(must_restrictions, may_restrictions, removed_restr):
    """
    removed_restr: list of restrictions that have been removed
    """
    for restr_src in list(may_restrictions):
        for restr in may_restrictions[restr_src]:
            if restr.pve_score >= done_score:
                # If this restriction has a high enough score (note: weak restriction are eliminated before)
                if restr.src in must_restrictions:
                    # If the src of the restr exists already
                    must_restrictions[restr.src].append(restr)
                else:
                    # If the src doesn't exist, add it with a new list as value
                    must_restrictions[restr.src] = [restr]
                # Remove the restriction from the relevant list in the dictionary
                may_restrictions[restr.src].remove(restr)
                removed_restr.append(restr)
            # If the negative score is more than the done limit, delete this
            elif restr.neg_score <= remove_score:
                may_restrictions[restr.src].remove(restr)
                removed_restr.append(restr)

        # If the corresponding list becomes empty, remove the key
        if len(may_restrictions[restr.src]) == 0:
            del may_restrictions[restr.src]

    return must_restrictions, may_restrictions, removed_restr


def score_restrictions(may_restrictions, r):
    """
    Given a restriction, updates the may restriction to include this or inc/dec score of this restriction.
    This method is called after every iteration.
    :param must_restrictions: current dict of must restrictions
    :param may_restrictions: current dict of may restrictions
    :param r: a restriction obtained in the latest iteration
    :return:
    """
    # If the src in r exists in may restrictions
    if r.src in may_restrictions:
        temp = 0
        # For each restr in the list of may restrictions for src
        for restr in may_restrictions[r.src]:
            temp = restr.compare(r)
            if temp == 1:
                # same, increase the positive score
                restr = restr.inc_score()
                restr = get_new_mul(restr)
                break
            elif temp == -1:
                # opposite, increase the negative score
                restr = restr.dec_score()
                restr = get_new_mul(restr)
                break
        # No entry with the same dest (the entire for loop is run for testing and each temp is 0)
        if temp == 0 and not r.weak:
            # Append the restriction to the relevant src entry (given it is not a weak restr)
            may_restrictions[r.src].append(r)
    elif not r.weak:
        # Add an entry for the src in r (given it is not a weak restr)
        may_restrictions[r.src] = [r]

    return may_restrictions


def find_restriction_cut(arg_indices_activated, vars_curr, removed_restr, result, ignore_src=[]):
    """
    For each argument currently used, finds a "LT" restriction based on variables already used
    result stores whether it was a successful or an unsuccessful cut.
    :param result: True if generated from a command that succeeded, False from a command that failed
    :param arg_indices_activated: The argument indices currently involved (list of lists)
    :param vars_curr: the values of the arguments used in the current iteration
    :param removed_restr: type: list the list of restrictions that have been removed, and need not be generated again
    :param ignore_src: arg_nums to ignore while finding restrictions (Note: Currently only applicable to the sorted mode)
    :return:
    """
    print("Removed restr", removed_restr)
    restrictions = []
    if not sort_mode_active:
        for i in range(len(arg_indices_activated[-1])):
            # For each index activated in the current op
            for elem_list in range(len(arg_indices_activated) - 1):
                # Test against all params in the test, except the last one
                for elem in range(len(arg_indices_activated[elem_list])):
                    # For each individual entry
                    if not result:
                        if vars_curr[int(arg_indices_activated[-1][i])] > vars_curr[elem]:
                            # Dictionary is overwriting the LT operator
                            restrictions.append(restriction(src=arg_indices_activated[-1][i],
                                                            rel="LT", dest=elem, dest_val=vars_curr[elem],
                                                            src_val=vars_curr[int(arg_indices_activated[-1][i])],
                                                            success=result))
                        else:
                            restrictions.append(restriction(src=arg_indices_activated[-1][i],
                                                            rel="GT", dest=elem, dest_val=vars_curr[elem],
                                                            src_val=vars_curr[int(arg_indices_activated[-1][i])],
                                                            weak=True, success=result))

                    else:
                        # If the current var is less than some preceeding var
                        if not vars_curr[int(arg_indices_activated[-1][i])] > vars_curr[elem]:
                            restrictions.append(restriction(src=arg_indices_activated[-1][i],
                                                            rel="LT", dest=elem, dest_val=vars_curr[elem],
                                                            src_val=vars_curr[int(arg_indices_activated[-1][i])],
                                                            weak=True, success=result))
                        else:
                            restrictions.append(restriction(src=arg_indices_activated[-1][i],
                                                            rel="GT", dest=elem, dest_val=vars_curr[elem],
                                                            src_val=vars_curr[int(arg_indices_activated[-1][i])],
                                                            weak=True, success=result))

    # A different strategy for restr generation for sort_mode
    else:
        # sort_max_steps is maximum number of steps to look back from the current step
        # Make a list that will store the various args that will be used to find restrs against
        dst_args = []
        # Loop that goes backward from the second last elem (till the beginning of the list or for sort_max_steps)
        for i in range(len(arg_indices_activated) - 2, max(len(arg_indices_activated) - sort_max_steps - 2, -1), -1):
            # Make a flattened list of important dst arguments
            dst_args = dst_args + arg_indices_activated[i]

        # Remove the args to be ignored for the src
        for ignore_elem in ignore_src:
            # For each elem that needs to be ignored, check and remove it from the current list
            if ignore_elem in arg_indices_activated[-1]:
                arg_indices_activated[-1].remove(ignore_elem)
        # TODO: Do removal inclusion checks at append points.

        # Add the standard restrictions
        for curr_elem in arg_indices_activated[-1]:
            # For each index activated in the current operation
            for dst_elem in dst_args:
                # Break from the loop if upper limit of predicates reached
                if len(restrictions) >= pred_upper_limit: break
                # For each index mandated to be checked against
                temp = restriction(src=curr_elem,
                                   rel="LT", dest=dst_elem,
                                   dest_val=vars_curr[int(curr_elem)] / vars_curr[int(dst_elem)],
                                   src_val=vars_curr[int(curr_elem)], success=result)
                # Only add the restriction if this hasn't already been removed
                if temp not in removed_restr: restrictions.append(temp)

        if enable_extended_predicates:
            # If there are combinations of dst_args possible
            if len(dst_args) >= 2:
                # TODO: Massive rewrite done. Error checking remains.

                # Build a more complicated list using sub as well.
                # Filter arg_c and arg_o by removing args that do not occur in dst_args.
                for arg_c in args_create:
                    if arg_c not in dst_args: continue
                    for arg_o in args_offset:
                        if arg_o not in dst_args: continue
                        dst_elem = sub(arg_c, arg_o)
                        dst_args.append(dst_elem)
                        # TODO: Add restrictions that are simply the sub based predicates.
                        for curr_elem in arg_indices_activated[-1]:
                            # For each index activated in the current operation
                            if len(restrictions) >= pred_upper_limit: break
                            # For each index mandated to be checked against
                            temp = restriction(src=curr_elem,
                                               rel="LT", dest=str(dst_elem),
                                               dest_val=vars_curr[int(curr_elem)] / dst_elem.eval(vars_curr),
                                               src_val=vars_curr[int(curr_elem)], is_extended=True,
                                               extended_dest=pickle.dumps(dst_elem), extended_mode="sub",
                                               success=result)
                            # Only add the restriction if this hasn't already been removed
                            if temp not in removed_restr: restrictions.append(temp)

                for comb_len in range(2, min(len(dst_args) + 1, 4)):
                    for comb_elem in itertools.combinations(dst_args, comb_len):
                        # comb_elem uses indices, we need to extract the values at these indices
                        comb_elem_eval = []
                        for elem in comb_elem:
                            if type(elem) is str:
                                comb_elem_eval.append(vars_curr[int(elem)])
                            elif type(elem) is sub:
                                comb_elem_eval.append(elem.eval(vars_curr))
                            else:
                                print("ERROR! Unknown type encountered!")
                        # The correnponding predicate evaluation is min (comb_elem)
                        pred_min = min(comb_elem_eval)
                        pred_max = max(comb_elem_eval)
                        # print("Using the combination, evaluation, value", comb_elem, pred_min, vars_curr[int(arg_indices_activated[-1][0])]/pred_min)

                        for i in range(len(arg_indices_activated[-1])):
                            # Break from the loop if upper limit of predicates reached
                            if len(restrictions) >= pred_upper_limit: break
                            # For each individual entry that is activated in the current step
                            temp = restriction(src=arg_indices_activated[-1][i],
                                               rel="LT", dest=str(comb_elem), dest_val=vars_curr[int(
                                    arg_indices_activated[-1][i])] / pred_min,
                                               src_val=vars_curr[
                                                   int(arg_indices_activated[-1][i])], is_extended=True,
                                               extended_dest=pickle.dumps(comb_elem), extended_mode="min",
                                               success=result)
                            if temp not in removed_restr: restrictions.append(temp)

                            # Max based predicates
                            temp = restriction(src=arg_indices_activated[-1][i],
                                               rel="LT", dest=str(comb_elem), dest_val=vars_curr[int(
                                    arg_indices_activated[-1][i])] / pred_max,
                                               src_val=vars_curr[
                                                   int(arg_indices_activated[-1][i])], is_extended=True,
                                               extended_dest=pickle.dumps(comb_elem), extended_mode="max",
                                               success=result)
                            if temp not in removed_restr: restrictions.append(temp)

    return restrictions


def find_restriction_dict_unb(args):
    """
    For each argument currently used, sets each of their restriction to unbounded.
    :param args: the list of indices activated currently

    :return: A dictionary with each var restricted to unbounded
    """
    restrictions = dict()
    for i in range(len(args)):
        # For each index activated in the current op
        # Note: We do not use the src value because it shouldn't matter
        restrictions[args[i]] = [restriction(src=args[i], rel="UNB")]
    return restrictions


def find_restriction_dict_lt(src, dest, src_val, dest_val):
    """
    Build a dictionary with a less than restriction
    :param src:
    :param dest:
    :return:
    """
    restrictions = dict()
    restrictions[src] = [restriction(src=src, dest=dest, rel="LT", src_val=src_val, dest_val=dest_val)]
    return restrictions


def update_must_dict(m_dict, restr):
    """
    If restr exists in m_dict, then nothing happens, else an entry is created.
    If there is a conflict a failure message is sent.
    :param m_dict:
    :param restr:
    :return:
    """
    if not m_dict:
        # If the dictionary is completely empty
        m_dict = restr
    else:
        for key in restr:
            if key in m_dict:
                # Note that there are a list of restrictions at the key
                for v in restr[key]:
                    # The value is a list (for each restriction in the list)
                    if v in m_dict[key]:
                        continue
                    else:
                        m_dict[key].append(v)
            else:
                m_dict[key] = restr[key]
    return m_dict


def get_random_vars(num_vars):
    """
    Creates a list of random vars of a specified size
    :param num_vars: The size of the list of vars that will be created
    :return: The list of random vars
    """
    v = []
    for i in range(num_vars):
        v.append(random.uniform(low_val, inf))
    return v


def get_new_mul(restr):
    """
    Given a may restriction, produces the next multiplier to be applied for sampling
    subject to the score of the restr (see vars move_up_score, move_below_score).
    :param restr:
    :return:
    """
    # Set a temp val
    restr_backup = None

    if restr.neg_score <= move_below_score:
        if restr.backup is not None:
            # If there is a prior value, use this; we are moving back
            restr = restr.backup
        else:
            # Change mul if the score is large enough
            if restr.rel == "GT":
                restr = restr.prev_mul()
            elif restr.rel == "LT":
                restr = restr.next_mul()

    elif restr.pve_score >= move_up_score:
        # Change mul if the score is large enough
        if restr.rel == "LT":
            restr = restr.prev_mul()
        elif restr.rel == "GT":
            restr = restr.next_mul()
    return restr


def barrier_func_sampling(prog_rep, singular=False, hybrid=False):
    """
    See https://en.wikipedia.org/wiki/Barrier_function.
    We use a cost that needs to be evaluated on the current list of samples.
    If the must restrictions are not met, then the cost -> inf.
    :return:
    """
    if not hybrid:
        # If not hybrid, do random sampling
        if not singular:
            prog_rep.args = get_random_vars(prog_rep.num_vars)
        else:
            prog_rep.args[prog_rep.state_onebyone.param_curr] = random.uniform(low_val, inf)
    # For each must restriction, sample locally anew if needed
    for key, val in prog_rep.restrictions_consolidated_must.items():
        for r in val:
            prog_rep.args = r.get_sample(prog_rep.args)
    return


class sub:
    """
    Represents a subtraction of indices a and b.
    """

    def __init__(self, a, b):
        self.a = a
        self.b = b

    def eval(self, dims):
        return dims[int(self.a)] - dims[int(self.b)]

    def tuple(self, dims):
        return [dims[int(self.a)], dims[int(self.b)]]

    def __len__(self):
        return 2

    def __repr__(self):
        return str(self.a) + " - " + str(self.b)

    def __hash__(self):
        return hash(self.__repr__())


class state_svm:
    def __init__(self, p_rep):
        # Reference to the prog_rep
        self.p_rep = p_rep
        # Argument samples in the form of X & Y matrices for each relevant restr. str->sample
        self.arg_samples = dict()
        # Completed SVMs
        self.svm_done = dict()
        # SVMs currently being built. str-> [svm, svm, ...]
        self.svm_ongoing = dict()

        # Some params to the way the stateful SVM stores, maintains and progresses
        # The training is done every train_it_mul iterations.
        self.train_it_mul = 1000
        # The minimum number of samples after which training is done.
        self.train_min_it = 5000
        # The maximum number of samples (after which no more SVMs are trained)->Best SVM so far moved to done
        self.max_samples = 8000
        # Cutoff accuracy, at which point no more SVMs are trained-> SVM exceeding cutoff moved to done
        self.accuracy_cutoff = 1.0
        # The modes for which no SVM needs to be constructed
        self.ignore_mode = ["CREATE", "EXTRUDE", "2D", "OFFSET", "2D_CUT"]

    def generate_new_vars(self):
        """
        Random samples for every variable involved if there is no finalized SVM.
        If there is a finalized SVM, then sample from the positive class.
        Note: If using hybrid mode, use this first as it cannot selectively modify params.
        :return:
        """
        # Flag that stores if some argument value does not correspond with the generated SVMs
        reset = True
        while reset:
            # Set random values to all vars
            arg_vals = []
            for i in range(self.p_rep.num_vars):
                arg_vals.append(random.uniform(low_val, inf))
            # Initially, assume this will pass
            reset = False
            # print("Size of svm_done", len(self.svm_done))
            for svm_id in self.svm_done:
                print("--------SVM Sampling from", svm_id)
                # If the prediction passes, continue to the next svm
                if self.make_prediction(s=self.svm_done[svm_id], args=arg_vals):
                    continue
                else:
                    reset = True
                    break
        # After checks, set the argument values to the prog_rep list
        self.p_rep.args = arg_vals

    def make_prediction(self, s, args):
        """
        Make a True / False prediction for the svm s using args.
        :param s: svm of type svm_struct
        :param args: list of argument values
        :return:
        """
        # TODO: Perform error checks.
        dims = self.get_svm_dims(activated_args=s.args_activated, args_offset=s.args_offset, args_create=s.args_create,
                                 args=args)
        prediction = s.svm.predict([dims])
        return prediction[0]

    def new_training_entry(self):
        """
        A new step has happened, let's add it to our structure
        :return:
        """
        # Temporary holder for variables involved
        if self.p_rep.mode not in self.ignore_mode:
            # Perhaps don't do anything if it was a create step.
            dims, labels = self.get_svm_dims(activated_args=self.p_rep.arg_indices_activated,
                                             args_create=list(args_create), args_offset=list(args_offset))
            # print("Args activated:", self.p_rep.arg_indices_activated)
            str_args_activated = str(self.p_rep.arg_indices_activated)
            # Add the entry to the dictionary depending on if prior entries for this exist or not.
            if str_args_activated in self.arg_samples:
                self.arg_samples[str_args_activated].add_sample(dims=dims, result=self.p_rep.curr_it_pass,
                                                                labels=labels)
            else:
                # Create a new sample data structure
                sample = self.sample_set(self.p_rep.arg_indices_activated)
                # Add the sample to the relevant location in the dictionary
                sample.add_sample(dims=dims, result=self.p_rep.curr_it_pass, labels=labels)
                self.arg_samples[str_args_activated] = sample

    def get_extended_dims(self, dims, args_create, args_offset, labels):
        """
        dims: The vector created without extended predicates so far; type: np.array
        args_create, args_offset: The arguments used in the create and offset mode for this sample(s); type: list
        labels: The labels of the extended dims, stored as a list of str.
        """
        # Build a large list including the offset vars in subtr. roles.
        # Create a copy of dims for internal use
        dims_temp = np.array(dims)
        # Initially a copy of the args_create list
        extended = list(args_create)
        for arg_c in args_create:
            for arg_o in args_offset:
                # print("Adding ", arg_c, "-", arg_o)
                extended.append(sub(arg_c, arg_o))
                # Added explicit sub based predicate to the SVM's dimension
                dims = np.append(dims, sub(arg_c, arg_o).eval(dims_temp))
                # Add relevant label
                labels.append(str(sub(arg_c, arg_o)))

        # Min on combinations of elements of the list
        for comb_len in range(2, min(len(extended) + 1, 4)):
            for comb in itertools.combinations(extended, comb_len):
                # We now have a combination of arg_nums
                comb_vals = []
                # TODO: Check and debug
                for elem in comb:
                    if type(elem) is str:
                        comb_vals.append(dims_temp[int(elem)])
                    elif type(elem) is sub:
                        comb_vals.append(elem.eval(dims_temp))
                    else:
                        print("ERROR! Unknown type encountered!")
                # Added min based param to the SVM's dimension
                dims = np.append(dims, min(comb_vals))
                labels.append("min of " + str(comb))

        return dims, labels

    def get_svm_dims(self, activated_args, args_offset, args_create, args=None):
        """
        Extracts arguments from the prog_rep, and also adds additional predicates depending on the context.
        :param args: The list of argument values. If None, use prog_rep

        :return:
        """
        if args is None:
            args = self.p_rep.args
        # Initialize dims as a numpy array
        dims = np.array([])

        # Initialize labels as a list
        labels = []
        # print("Activated args are:", activated_args)
        # Go over the activated args (Each step, each arg).
        # Additionally, a check is done if activated_args is 1D or 2D
        if not isinstance(activated_args[0], list):
            # activated_args is 1D
            for arg in activated_args:
                # print("Adding from 1D", arg)
                dims = np.append(dims, args[int(arg)])
                labels.append(arg)
        else:
            # activated_args is 2D
            for step in activated_args:
                for arg in step:
                    # print("Adding from 2D", arg)
                    dims = np.append(dims, args[int(arg)])
                    labels.append(arg)
        # Add more dimensions representative of min of combinations of arguments that have happened so far.
        dims, labels = self.get_extended_dims(dims, args_create, args_offset, labels=labels)
        return dims, labels

    def check_foresake_svm(self):
        """
        Remove samples / SVMs for arguments that have good restrictions available.
        :return:
        """
        del_keys = []
        # Check for each key if it can be foresaken
        for key in self.arg_samples:
            # Flatten the list of arguments involved in the key
            args_flattened = []
            for arg_line in self.arg_samples[key].args_activated:
                for arg in arg_line:
                    args_flattened.append(arg)
            # Check for deletion
            del_sample = True
            # For each elem in the flattened list, check if it occurs in must restrictions
            for elem in args_flattened:
                if elem not in self.p_rep.restrictions_consolidated_must.keys():
                    del_sample = False
                    break
            if del_sample:
                # Add the key to delete to a list for future deletion
                del_keys.append(key)

        for key in del_keys:
            del self.arg_samples[key]
            # if key in self.svm_done.keys(): del self.svm_done[key]
            if key in self.svm_ongoing.keys(): del self.svm_ongoing[key]

    def fin_it(self):
        """
        Run at the end of each iteration of the sampling.
        If the number of samples we have is a multiple of sort_min_samples, train a new SVM.
        TODO: Update accuracy metrics after each new SVM creation for all SVMs prior.
        TODO: If using an SVM from the done struct, and it fails, add the new sample point, demote the done back to ongoing.
        :return: True if ready to end sampling, False otherwise
        """
        # Check if certain keys should be foresaken in lieu of restrictions generation in the sort based mode
        self.check_foresake_svm()

        for key in self.arg_samples:
            # Train an SVM, promote / demote SVMs to / from the ongoing and done structures
            if (len(self.arg_samples[key]) % self.train_it_mul == 0) and (len(self.arg_samples[key]) >
                                                                          self.train_min_it) and self.arg_samples[
                key].valid:
                # Ensure that training for specific SVMs does not happen again and again
                if key in self.svm_ongoing:
                    n = int(len(self.arg_samples[key]) / sort_min_samples)
                    if len(self.svm_ongoing[key]) >= n:
                        continue
                # Train an SVM
                print("We will train an SVM for", key)
                s = self.train_test(key=key)
                if key not in self.svm_done:
                    if key in self.svm_ongoing:
                        self.svm_ongoing[key].append(s)
                    else:
                        # If key is not ongoing, add it if this is not already completely done.
                        self.svm_ongoing[key] = [s]
                    # Check if this should be moved to self.svm_done
                    # If the accuracy of the SVM is already greater than the threshold, move it to done.
                    if self.svm_ongoing[key][-1].accuracy >= self.accuracy_cutoff:
                        self.svm_done[key] = self.svm_ongoing[key][-1]
                        # Remove the key from svm_ongoing
                        del self.svm_ongoing[key]
                        continue
                    # If there are more than max num. of samples collected, select the SVM with the highest accuracy
                    # and move it to done.
                    if len(self.arg_samples[key]) >= self.max_samples:
                        s_highest_accuracy = None
                        # Find the svm with the highest accuracy so far
                        for s in self.svm_ongoing[key]:
                            if s_highest_accuracy is None or s.accuracy >= s_highest_accuracy.accuracy:
                                s_highest_accuracy = s
                        self.svm_done[key] = s_highest_accuracy
                        # Remove the key from svm_ongoing, and continue with the loop
                        del self.svm_ongoing[key]

        print("SVM ongoing", self.svm_ongoing)
        print("SVM done", self.svm_done)

        # Naive test for end of sampling
        for key in self.arg_samples:
            if key not in self.svm_done:
                return False
        print("Time to end SVM based synthesis!")
        return True

    def train_test(self, key, test_size=0.25):
        """
        Train and test the SVM with a subset of the X and Y samples.
        :param key: The key is used to obtain X and Y matrices from self.arg_samples
        :param test_size: the proportion of gathered data to use as testing set
        :return: the trained model and additional info in the type svm_struct
        """
        # An RBF kernel does not buy much, and takes away explainability.
        # A higher C parameter ensures a wider decision boundary.
        # A higher value of gamma ensures more samples are considered together to build a decision boundary.
        clf = svm.SVC(kernel='linear')

        # Extract relevant X and Y
        X = self.arg_samples[key].X
        Y = self.arg_samples[key].Y

        # The exact number of test samples
        test_num = int(test_size * len(X))
        # Select indices of the test samples
        sel = random.sample(range(0, len(X)), test_num)

        testing_X = np.take(X, sel, axis=0)
        testing_Y = np.take(Y, sel)

        training_X = np.delete(X, sel, axis=0)
        training_Y = np.delete(Y, sel)

        clf.fit(training_X, training_Y)
        # print("We have an SVM for", self.arg_samples[key].args_activated)
        s = self.svm_struct(svm=clf, samples=self.arg_samples[key], accuracy=clf.score(testing_X, testing_Y))
        return s

    # A class that encapsulates many SVM (training / testing) samples
    class sample_set:
        def __init__(self, args_activated):
            """
            Initializes a structure for holding samples for specific operations/lines in the CAD program.
            :param args_activated: The args activated for this sample set (as a list of lists)
            """
            # X & Y will be later instantiated and used as numpy arrays
            self.X = None
            self.Y = None

            # Labels to the SVM coefficients
            self.coeff_labels = None

            self.args_activated = copy.deepcopy(args_activated)
            # Snapshots of the create and offset args
            self.args_create = list(args_create)
            self.args_offset = list(args_offset)

            # Store the validity of the structure to be used for training (are there both, True and False samples)
            self.valid = False

        def __len__(self):
            if self.Y is not None:
                return np.alen(self.Y)
            else:
                return 0

        def __repr__(self):
            """
            Pretty print the restriction representation
            """
            return "X:" + str(self.X) + "\tY:" + str(self.Y)

        def add_sample(self, dims, result, labels=None):
            """
            Adds a new sample to the sample set (appends to X and Y).
            :param dims:
            :param result:
            :return:
            """
            # print("New sample for", self.args_activated)
            if self.X is None or self.Y is None:
                # If not initialized
                self.X = np.array(dims)
                self.Y = np.array(result)
                self.coeff_labels = labels
            else:
                # save data, arrays already initialized
                self.X = np.vstack((self.X, dims))
                self.Y = np.append(self.Y, result)
                # Check if this new addition makes the sample struct valid
                if not self.valid: self.check_valid()

        def check_valid(self):
            # Does a validity check and sets the relevant var
            if self.Y[-2] != self.Y[-1]:
                self.valid = True

        def num_samples(self):
            if self.Y is not None:
                return np.alen(self.Y)
            else:
                return 0

    class svm_struct:
        def __init__(self, svm, samples, accuracy):
            self.svm = svm
            self.args_activated = samples.args_activated
            # TODO: Snapshots of create and offset
            self.args_offset = list(samples.args_offset)
            self.args_create = list(samples.args_create)
            self.accuracy = accuracy
            self.len_samples = len(samples.Y)
            self.coeff_labels = samples.coeff_labels
            print("New SVM for", str(self.args_activated), "with accuracy:", self.accuracy, "using", self.len_samples,
                  "samples constructed.")
            # Interpreting an SVM: https://stats.stackexchange.com/questions/39243/how-does-one-interpret-svm-feature-weights
            # print("Coefficients", self.svm.coef_, "Intercept", self.svm.intercept_, "Labels", self.coeff_labels)
            print("The top weighted predicates are:", self.print_top_coeff())

        def print_top_coeff(self, n=5):
            top_indices = np.argpartition(self.svm.coef_[0], -n)[-n:]
            ret = ""
            for ind in top_indices:
                ret = ret + str(self.coeff_labels[ind]) + " with " + str(self.svm.coef_[0][ind]) + "\t"
            return ret

        def __repr__(self):
            """
                    Pretty print the restriction representation
                    """
            return "Samples " + str(self.len_samples) + ", accuracy " + str(self.accuracy)
