# To make sort + sub restrictions work
import constraints
import itertools
import random
import pickle
import pulp
import copy


class state_lp:
    def __init__(self, p_rep):
        """
        Initializes the state of lp based synthesis
        :param p_rep: the prog_representation structure
        """
        # Hypotheses being tested (str -> lp_hypothesis)
        self.hypotheses = dict()
        # Reference to the prog_rep structure
        self.p_rep = p_rep
        # TODO: Store the falsified hypotheses so as to not worry about them again.
        self.removed = set()
        # Almost correct solutions, the multipliers need to be adjusted.
        self.curr_tentative = dict()
        # The final restrictions obtained
        self.final_soln = dict()

    def add_hypothesis_sample(self, result, use_prev=False, for_args=None):
        """
        Add a new hypothesis sample. Also sets up the hypotheses, if not already done.
        :param result: True or False, depending on pass or fail
        :param use_prev: Use unresolved args, or set offsets to unbounded, rather than currently activated args
        :param
        :return:
        """

        if use_prev:
            for o in constraints.args_offset:
                self.p_rep.restrictions_consolidated_must = constraints.update_must_dict(self.p_rep.restrictions_consolidated_must,
                                                                               constraints.find_restriction_dict_unb(o))
            # Cut based 2D operations
            curr_args = constraints.args_cut
        else:
            #TODO: Add provision to also directly get curr_args from caller.
            if for_args is None:
                curr_args = self.p_rep.arg_indices_activated[-1]
            else:
                curr_args = for_args
        # For each arg activated in the current step, do the following
        for arg in curr_args:
            if arg in self.hypotheses:
                self.hypotheses[arg].add_sample(result=result)
            elif arg not in self.removed:
                self.hypotheses[arg] = lp_hypothesis(src=arg, state_lp_ref=self, activated_args=curr_args)
                self.hypotheses[arg].add_sample(result=result)
            """
            else:  # The arg is removed and we don't need to add it again.
                pass
            """

    def generate_vars(self):
        """
        Generates vars either naively, or smartly based on self.curr_tentative.
        :return: A list of args that shall be used in the current iteration.
        """
        print("---(sampling for new LP mode iteration)---")
        args = []
        # First sample naively
        for i in range(self.p_rep.num_vars):
            args.append(random.uniform(constraints.low_val, constraints.inf))

        # Then set args to what must restrictions demand
        for key, val in self.p_rep.restrictions_consolidated_must.items():
            for r in val:
                print("Sampling using", str(r))
                args = r.get_sample(args)

        # Then modify arguments occuring in tentative restrictions for fine tuning multipliers.
        for rstr in self.curr_tentative:
            print("Using", self.curr_tentative[rstr], " to get a better sample.")
            args = self.curr_tentative[rstr].get_sample_near(args)
        return args

    def fin_it_lp(self):
        """
        Runs after every LP iteration.
        :return:
        """
        # Reset the removal list
        removal = []
        old_tentative = copy.copy(self.curr_tentative)
        new_solves = False
        # For each hypothesis in the ongoing examination, solve every few steps (p_rep.min_samples).
        for key in self.hypotheses:
            if len(self.hypotheses[key]) % self.p_rep.min_samples == 0:
                # There will be some solving in the iteration
                new_solves = True
                print("Will check on restrictions for", key)
                r = self.hypotheses[key].lp_manager()
                if type(r) == constraints.restriction:
                    self.curr_tentative[key] = r
                elif r == -1:
                    print("No possible restriction found for", key)
                    removal.append(key)
                # Update the state of solve for the hypothesis
                self.hypotheses[key].analyze_state_of_solve()

            # If there are more samples for this hypotyhesis than the max needed, then remove the hypothesis, or move to
            # must if a possible solution is known.
            if len(self.hypotheses[key]) > self.p_rep.max_samples:
                # If there is a tentative restriction for this key, move it to must
                if key in self.curr_tentative:
                    print("Moving", self.curr_tentative[key], " to a must restriction.")
                    self.p_rep.restrictions_consolidated_must = constraints.add_rstr_to_must(self.curr_tentative[key],
                                                                                             self.p_rep.restrictions_consolidated_must)
                # If there is no tentative restriction for this key, use the top most solution we have so far
                else:
                    temp = list(self.hypotheses[key].passing.keys())[0]
                    print("Using the approximate solution...", temp, "from", self.hypotheses[key].passing.keys())
                    self.p_rep.restrictions_consolidated_must = constraints.add_rstr_to_must(temp,
                                                                                             self.p_rep.restrictions_consolidated_must)
                removal.append(key)

        if new_solves:
            for key in self.curr_tentative:
                if key in old_tentative:
                    if self.curr_tentative[key] == old_tentative[key]:
                        # Check with earlier removal when to do this
                        print("Improved tentative solution: ", self.curr_tentative[key])
                else:
                    print("New tentative solution: ", self.curr_tentative[key])

        # Delete the requisite keys
        for key in removal:
            print("Deleting hypothesis", key)
            if key in self.hypotheses:
                del self.hypotheses[key]
            else:
                print("How are we trying to remove something that should not exist?")
            if key in self.curr_tentative:
                del self.curr_tentative[key]
            # Add key to internal structure so that it is not added again
            self.removed.add(key)

        print("Params we currently have hypotheses for", self.hypotheses.keys())

        print("---(end of LP mode iterations)---")

    def check_end(self):
        """
        Checks whether we are done.
        :return: True if done, False if not
        """
        # If there are no more hypotheses left, we are done.
        if len(self.hypotheses) == 0:
            print("---(end of ALL LP mode iterations, no hypotheses left)---")
            info = "\nMust restrictions:\t"
            # For each param
            for param in self.p_rep.restrictions_consolidated_must:
                # For each restriction for this param
                for r in self.p_rep.restrictions_consolidated_must[param]:
                    info = info + "\n" + str(r)
            if self.p_rep.out_file is None:
                print(info)
            else:
                self.p_rep.out_file.write(info)
            return True
        else:
            return False


class lp_hypothesis:
    """
    rstr types used: min_2c (minimum based constraints with different constants to the args inside the min);
    min_sub (minimum based constraints that have a subtraction inside the min).
    """

    def __init__(self, state_lp_ref, src, activated_args=None, pred_gen_mode=3):
        """

        :param state_lp_ref: Ref to the state object
        :param src: The src of restriction (LHS)
        :param pred_gen_mode: The level at which predicates are generated (>0 : basic predicates, >1 : subtractive
        predicates, >2 : predicates with 2 constants.
        """
        print("Creating LP Hypothesis with a new sample")
        self.state_lp_ref = state_lp_ref
        # Passing and failing dicts will store respective values for predicates under test (restriction -> [])
        self.passing = dict()
        self.failing = dict()
        self.src = src
        self.state_of_solve = 0  # Corresponds to the current solve state so as to not solve excessively (0 to 2).

        # Set the args used the current op (to eliminate getting a rstr of a var against itself)
        if activated_args is None:
            act_args = state_lp_ref.p_rep.arg_indices_activated[-1]
        else: act_args = activated_args
        """
        Add the possible restrictions we may need to check
        """
        # Construct a flat list of all arguments occuring before (taking out the last/current step).
        flat_args_prev = [e for e in state_lp_ref.p_rep.arg_indices_activated_flat if e not in act_args]
        if pred_gen_mode > 0:
            # Simple comparison to 1 var
            for arg_prev in flat_args_prev:
                rstr = constraints.restriction(src=self.src, rel="LT", dest=str(arg_prev))
                self.passing[rstr] = []
                self.failing[rstr] = []

        if pred_gen_mode > 1:
            # Add specifically the subtractive predicates to the flat_args_prev list
            for arg_c in constraints.args_create:
                for arg_o in constraints.args_cut:
                    # Eliminate possibility of using the src in the comparison predicate
                    if arg_c != self.src and arg_o != self.src:
                        # Added explicit 'sub' based predicate to the dimension
                        e = constraints.sub(arg_c, arg_o)
                        # rstr = constraints.restriction(src=self.src, rel="LT", dest=str(e),
                        #                    extended_dest=pickle.dumps(e), is_extended=True, extended_mode="sub")
                        # self.passing[rstr] = []
                        # self.failing[rstr] = []
                        flat_args_prev.append(e)

            if len(flat_args_prev) >= 2:
                for comb_len in range(2, min(len(flat_args_prev) + 1, 4)):  # Max combination limited
                    for comb_elem in itertools.combinations(flat_args_prev, comb_len):
                        # Flag to check if a sub type is involved in the combination
                        temp_elems_not_sub = []
                        temp_sub = []
                        temp_flag = False
                        temp_skip = False
                        for e in comb_elem:
                            if type(e) == constraints.sub:
                                temp_flag = True
                                temp_sub.append(e.a)
                                temp_sub.append(e.b)
                            else:
                                temp_elems_not_sub.append(e)
                        # Skipping some combinations due to repetitiveness.
                        for e in temp_sub:
                            if e in temp_elems_not_sub:
                                temp_skip = True
                        if temp_flag and not temp_skip:
                            rstr = constraints.restriction(src=self.src, rel="LT", dest=str(comb_elem),
                                                extended_dest=pickle.dumps(comb_elem), is_extended=True, extended_mode="min_sub")
                            self.passing[rstr] = []
                            self.failing[rstr] = []

                        elif not temp_flag:
                            rstr = constraints.restriction(src=self.src, rel="LT", dest=str(comb_elem),
                                               extended_dest=pickle.dumps(comb_elem), is_extended=True, extended_mode="min")
                            self.passing[rstr] = []
                            self.failing[rstr] = []

                            # same for max
                            rstr = constraints.restriction(src=self.src, rel="LT", dest=str(comb_elem), extended_dest=pickle.dumps(comb_elem), is_extended=True, extended_mode="max")
                            self.passing[rstr] = []
                            self.failing[rstr] = []

        if pred_gen_mode > 2:
            """
            More complicated restrictions of the kind min(c1. y1, c2. y2,...).
            There is more than one constant involved.
            """
            if len(flat_args_prev) >= 2:
                for comb_len in range(2, min(len(flat_args_prev) + 1, 4)):  # Max combination of 'min' args
                    for comb_elem in itertools.combinations(flat_args_prev, comb_len):
                        # Flag to check if a sub type is involved in the combination
                        temp_elems_not_sub = []
                        temp_sub = []
                        temp_flag = False
                        temp_skip = False
                        for e in comb_elem:
                            if type(e) == constraints.sub:
                                temp_flag = True
                                temp_sub.append(e.a)
                                temp_sub.append(e.b)
                            else:
                                temp_elems_not_sub.append(e)
                        # Skipping some combinations due to repetitiveness.
                        for e in temp_sub:
                            if e in temp_elems_not_sub:
                                temp_skip = True
                        if not temp_flag or (temp_flag and not temp_skip):
                            rstr = constraints.restriction(src=self.src, rel="LT", dest=str(comb_elem),
                                               extended_dest=pickle.dumps(comb_elem), is_extended=True, extended_mode="min_2c")
                            self.passing[rstr] = []
                            self.failing[rstr] = []

        # Set the sample counter to 0
        self.samples = 0

    def add_sample(self, result):
        # print("Adding LP Hypothesis sample", result)
        x = self.state_lp_ref.p_rep.args[int(self.src)]
        # Add the sample for all current hypotheses
        for rstr in self.passing:
            y = rstr.eval_val(self.state_lp_ref.p_rep.args)
            #if rstr.extended_mode == "min_2c" and int(self.src) == 3:
                #print("Considering ", rstr, ": ", y)
            if result:
                self.passing[rstr].append((x, y))
            else:
                self.failing[rstr].append((x, y))
        self.samples = self.samples + 1

    def analyze_state_of_solve(self):
        """
        Look at each restriction we have, and increment self.state_of_solve when appropriate.
        """
        # Temp set that collects the extended modes we still have
        temp = set()
        for rstr in self.passing.keys():
            if rstr.extended_mode == "":
                temp.add(0)
            elif rstr.extended_mode in ["min", "max", "sub", "min_sub"]:
                temp.add(1)
            elif rstr.extended_mode == "min_2c":
                temp.add(2)

        # If there are still rstr to check in lower modes, stay there, else move up
        if self.state_of_solve == 0:
            if 0 in temp:
                return
            else:
                print("Will solve more complicated restrictions (min, max, sub) for", self.src)
                self.state_of_solve = 1
                return

        elif self.state_of_solve == 1:
            if 1 in temp:
                return
            else:
                print("Will solve more complicated restrictions (2 multipliers) for", self.src)
                self.state_of_solve = 2
                return

        elif self.state_of_solve == 2:
            # No higher state of solving
            return

    def compatible_state_of_solve(self, rstr):
        """
        Says if the mode of solving of given rstr is allowed by self.state_of_solve
        :return: True or False
        """
        if self.state_of_solve == 0:
            if rstr.extended_mode == "":
                return True
            else:
                return False
        elif self.state_of_solve == 1:
            if rstr.extended_mode in ["min", "max", "sub", "min_sub"]:
                return True
            else:
                return False
        elif self.state_of_solve >= 2:
            return True

    def lp_manager(self):
        """
        :return: 0 (many restrictions remain), -1 (nothing found), or the restriction itself if only one is left
        """
        deletion = []
        success = []
        # For each possible restriction collected so far, solve and update.
        for rstr in self.passing:
            # Only solve for the rstr if we are in a compatible state of solving (to disallow solving complex rstr).
            if self.compatible_state_of_solve(rstr):
                print("Now considering", str(rstr), "of type", rstr.extended_mode)
                m = self.solve(self.passing[rstr], self.failing[rstr], rstr)
                if type(m) is int:
                    print("Solver unsuccessful for ", str(rstr))
                    # We fail and quit even if we can't compute due to some strange reason.
                    deletion.append(rstr)
                else:
                    # Solution found
                    print("Multiplier changed from", rstr.mul, "to", m)
                    rstr.mul = m
                    print("Solver successful for ", str(rstr))
                    # Update multiplier and add to the success list
                    success.append(rstr)
            else:
                print("Skipping", rstr, ", may come back later.")

        # Delete restrs. that can't be solved
        for r in deletion:
            del self.passing[r]
            del self.failing[r]

        if len(success) == 1:
            # The restriction that is valid for the src arg
            return success[0]
        elif len(success) > 1:
            # Need more turns to eval
            return 0
        else:
            # No restriction worked
            if self.state_of_solve < 2:
                return 0
            else:
                return -1

    def solve(self, passing, failing, rstr):
        """
        Builds LP equations and calls the solver.
        :param passing: list of passing vals (x < c. y gives [x, y]. Note that y itself may be a list.)
        :param failing: list of failing vals (x < c. y gives [x, y]. Note that y itself may be a list.)
        :param rstr: The restriction object being used / evaluated
        :return: res, c : result is 1 (success), 0 (weird) or -1 (fail), c is the constant found, or None
        """
        prob = pulp.LpProblem("restr_solver")

        if rstr.extended_mode == "min_2c":
            # A list enumerating the number of c's we need
            n_rhs = range(rstr.extended_len)
            c = pulp.LpVariable.dicts(name="c", indexs=n_rhs, lowBound=0.1, upBound=4)

            M = 99999  # Some large value

            n_failing = range(len(failing))

            # The bounds of s help select among one of the components of the disjunction
            s = pulp.LpVariable.dicts(name="s", indexs=(n_failing, n_rhs), lowBound=0, upBound=1, cat=pulp.LpInteger)

            # Each passing entry conforms to the restriction
            for entry in passing:
                for n in n_rhs:
                    prob += entry[0] <= c[n] * entry[1][n]

            for i in n_failing:
                # The entry is given by the current index of the (outer) loop.
                entry = failing[i]
                tmp = 0
                for n in n_rhs:
                    prob += entry[0] - c[n] * entry[1][n] >= -M * s[i][n]
                    # One of the s[i]s need to be activating (value 0)
                    tmp = tmp + s[i][n]
                prob += tmp <= rstr.extended_len - 1

            status = prob.solve()

            # The solution is given by all values of c
            soln = []
            for n in n_rhs:
                soln.append(c[n].value())

        elif rstr.extended_mode is "min_sub":
            # Restrictions based on the combination of min and sub, with constants for the sub.

            # Create a list of the positions of subs by using the first passing entry as a proxy.
            sub_pos = []
            # Check for index error
            if len(passing) > 0:
                temp = passing[0][1]
                for i in range(len(temp)):
                    # The subs appear as lists
                    if type(temp[i]) is list: sub_pos.append(i)

                # A list enumerating the number of c's we need
                n_rhs = range(rstr.extended_len)
                # The constant multipliers for the args to min.
                # c = pulp.LpVariable.dicts(name="c", indexs=n_rhs, lowBound=0.01, upBound=9)
                # The constant multipliers for the subtractant quantity
                d = pulp.LpVariable.dicts(name="d", indexs=sub_pos, lowBound=0.1, upBound=4)

                M = 99999  # Some large value

                n_failing = range(len(failing))

                # The bounds of s help select among one of the components of the disjunction
                s = pulp.LpVariable.dicts(name="s", indexs=(n_failing, n_rhs), lowBound=0, upBound=1, cat=pulp.LpInteger)

                # Each passing entry conforms to the restriction
                for entry in passing:
                    for n in n_rhs:
                        if n in sub_pos:
                            e = entry[1][n]
                            prob += entry[0] <= e[0] - d[n] * e[1]
                        else:
                            # Add more and more arguments in the min
                            prob += entry[0] <= entry[1][n]

                for i in n_failing:
                    # The entry is given by the current index of the (outer) loop.
                    entry = failing[i]
                    tmp = 0
                    for n in n_rhs:
                        if n in sub_pos:
                            e = entry[1][n]
                            prob += entry[0] - (e[0] - d[n] * e[1]) >= -M * s[i][n]
                            # One of the s[i]s need to be activating (value 0)
                            tmp = tmp + s[i][n]
                        else:
                            prob += entry[0] - entry[1][n] >= -M * s[i][n]
                            # One of the s[i]s need to be activating (value 0)
                            tmp = tmp + s[i][n]
                    prob += tmp <= rstr.extended_len - 1

                status = prob.solve()

                # The solution is given by all values of c
                soln = []
                for n in sub_pos:
                    soln.append(d[n].value())
            else: status = -1

        else:
            # The generic case with 1 constant to the predicate
            # The constant is unknown and needs to be found
            c = pulp.LpVariable("c", lowBound=0.1, upBound=4)
            # Add passing samples
            for entry in passing:
                prob += entry[0] <= c * entry[1]

            # Failing samples
            for entry in failing:
                prob += entry[0] >= c * entry[1]

            status = prob.solve()
            soln = c.value()

        if status == pulp.LpStatusOptimal:
            return soln
        # -1 is for manual elimination if no diversity in evidence (+ and - samples)
        elif status in [pulp.LpStatusInfeasible, pulp.LpStatusUndefined, -1]:
            print("LP failed")
            return -1
        else:
            print("Something weird happened in the solve process", status)
            return 0

    def __len__(self):
        return self.samples
