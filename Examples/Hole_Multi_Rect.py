# Name: Hole_Multi_Rect Number of arguments: 5
import cadquery as cq

print("----", "Using", __file__, "-----")


def main(prog_rep=None):
    """
    The main structure which creates the design
    :param args: The arguments to the design in the form of a list
    :return:
    """

    prog_rep.change_mode(mode="CREATE", arg_nums=["0", "1", "2"])
    box = cq.Workplane("XY").box(prog_rep.args[0], prog_rep.args[1], prog_rep.args[2])

    prog_rep.change_mode(mode="2D", arg_nums=["3"], prev=box, intermediate=box.faces(">Z"))
    result = box.faces(">Z").workplane().rect(prog_rep.args[3], prog_rep.args[3],
                                              forConstruction=True)

    prog_rep.change_mode(mode="HOLE", arg_nums=["4"], prev=result)
    result = result.vertices().hole(prog_rep.args[4])

    if prog_rep.args[4] > min(prog_rep.args[0] - 2 * prog_rep.args[3], prog_rep.args[1] - 2 * prog_rep.args[3]):
        print("GOGO")

    # Displays the result of this script
    # Export STL
    #f = open('Exports\\ExampleCQ003.stl', 'w')
    #cq.exporters.exportShape(shape=result, fileLike=f, exportType="STL", tolerance=0.01)
    #f.close()
    return result