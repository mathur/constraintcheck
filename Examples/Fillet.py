# Examples.Fillet num of args: 5
#

import cadquery as cq


def main(prog_rep=None):
    """
    The main structure which creates the design
    :param args: The arguments to the design in the form of a list
    :return:
    """

    prog_rep.change_mode(mode="CREATE", arg_nums=["0", "1", "2"])
    result = cq.Workplane("XY").box(prog_rep.args[0], prog_rep.args[1], prog_rep.args[2])

    prog_rep.change_mode(mode="FILLET", arg_nums=["3"], prev=result)
    result = result.faces(">Z").fillet(prog_rep.args[3])

    prog_rep.change_mode(mode="HOLE", arg_nums=["4"], prev=result)
    result = result.faces(">Z").workplane().hole(prog_rep.args[4])

    return result
