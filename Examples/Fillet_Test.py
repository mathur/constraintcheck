# A simple shape based on a box with a hole and fillet
import cadquery as cq
# [67.58756005631156, 42.47130056981769, 77.55913203387081, 31.60423684268781]
# [41.29277112980852, 23.14549478163227, 12.629814052355663, 9.653698741731548]
args = [50, 50, 50,  43.174799]

result = cq.Workplane("XY").box(args[0], args[1], args[2])
result = result.faces(">Z").fillet(args[3])

# Export STL
f = open('Fillet_01.stl', 'w')
cq.exporters.exportShape(shape=result, fileLike=f, exportType="STL", tolerance=0.01)
f.close()
