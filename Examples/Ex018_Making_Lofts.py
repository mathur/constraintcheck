# Name: Ex018_Making_Lofts Number of arguments: 7
# Does not have any interesting restrictions
import cadquery as cq
print("----", "Using", __file__, "-----")
# Create a lofted section between a rectangle and a circular section.
# 1.  Establishes a workplane that an object can be built on.
# 1a. Uses the named plane orientation "front" to define the workplane, meaning
#     that the positive Z direction is "up", and the negative Z direction
#     is "down".
# 2.  Creates a plain box to base future geometry on with the box() function.
# 3.  Selects the top-most Z face of the box.
# 4.  Draws a 2D circle at the center of the the top-most face of the box.
# 5.  Creates a workplane 3 mm above the face the circle was drawn on.
# 6.  Draws a 2D circle on the new, offset workplane.
# 7.  Creates a loft between the circle and the rectangle.

def main(prog_rep=None):
    """
    The main structure which creates the design
    :param args: The arguments to the design in the form of a list
    :return:
    """

    prog_rep.change_mode(mode="CREATE", arg_nums=["0", "1", "2"])
    result = cq.Workplane("front").box(prog_rep.args[0], prog_rep.args[1], prog_rep.args[2])

    prog_rep.change_mode(mode="2D", arg_nums=["3"])
    result = result.faces(">Z").circle(prog_rep.args[3])

    prog_rep.change_mode(mode="OFFSET", arg_nums=["4"])
    result = result.workplane(offset=prog_rep.args[4])

    prog_rep.change_mode(mode="2D", arg_nums=["5", "6"])
    result = result.rect(prog_rep.args[5], prog_rep.args[6])

    prog_rep.change_mode(mode="LOFT", prev=result)
    result = result.loft(combine=True)

    return result

