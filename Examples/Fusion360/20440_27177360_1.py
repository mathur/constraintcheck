# Name: 20440_27177360_1 Number of arguments: 10
# Taken from Fusion 360 Segmentation dataset

import cadquery as cq

print("----", "Using", __file__, "-----")


def main(prog_rep=None):
    """
    The main structure which creates the design
    :param args: The arguments to the design in the form of a list
    :return:
    """

    prog_rep.change_mode(mode="CREATE", arg_nums=["0", "1", "2"])
    base = cq.Workplane("XY").box(prog_rep.args[0], prog_rep.args[1], prog_rep.args[2])

    prog_rep.change_mode(mode="FILLET", arg_nums=["3"], prev=base)
    base = base.edges("|Z").fillet(prog_rep.args[3])

    # Get vertex where the cut is going to be made (Note: faces(">Z") is not working)
    prog_rep.change_mode(mode="VERT_ON", arg_nums=["4"], prev=base)
    base2 = cq.Workplane("XY").transformed(offset=(prog_rep.args[4], 0, prog_rep.args[2]/2))

    # Create boxes on the chosen vertices, the depth needs to be the depth of the initial box (through-hole)
    prog_rep.change_mode(mode="CREATE", arg_nums=["5"], prev=base2, intermediate=base, alt=True)
    base2 = base2.box(prog_rep.args[5], prog_rep.args[5], prog_rep.args[2] + 0.1, combine=False)
    # Then the mirror. It is not currently suported, but the mirror will succeed if the previous succeeds
    base3 = cq.Workplane("XY").transformed(offset=(-prog_rep.args[4], 0, prog_rep.args[2] / 2))
    base3 = base3.box(prog_rep.args[5], prog_rep.args[5], prog_rep.args[2] + 0.1)
    base2 = base2.union(base3)

    prog_rep.change_mode(mode="CUT", prev=base2, alt=True)
    base = base.cut(base2)

    prog_rep.change_mode(mode="CREATE", arg_nums=["6", "7", "8"], prev=base, alt=True)
    base2 = cq.Workplane("XY").box(prog_rep.args[6], prog_rep.args[7], prog_rep.args[8])

    prog_rep.change_mode(mode="FILLET", arg_nums=["9"], prev=base2, alt=True)
    base2 = base2.edges("|X").fillet(prog_rep.args[9])

    prog_rep.change_mode(mode="UNION", arg_nums=["9"], prev=base2, alt=True)
    base = base.union(base2)

    return base