# Name: 20260_4a01a99d_8 Number of arguments: 4
# Taken from Fusion 360 Segmentation dataset

import cadquery as cq
print("----", "Using", __file__, "-----")


def main(prog_rep=None):
    """
    The main structure which creates the design
    :param args: The arguments to the design in the form of a list
    :return:
    """

    prog_rep.change_mode(mode="CREATE", arg_nums=["0", "1", "2"])
    base = cq.Workplane("XY").box(prog_rep.args[0], prog_rep.args[1], prog_rep.args[2])

    prog_rep.change_mode(mode="CREATE", arg_nums=["0"])
    base2 = base.vertices(">Y and <X and <Z").transformed(offset=(prog_rep.args[0] / 2, 0, 0)).circle(prog_rep.args[0] / 2)

    prog_rep.change_mode(mode="EXTRUDE", arg_nums=["0"], prev=base)
    base2 = base2.extrude(prog_rep.args[2])

    base = base.union(base2)

    return base