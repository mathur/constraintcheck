# Name: 20163_3742f394_0 Number of arguments: 4
# Taken from Fusion 360 Segmentation dataset

import cadquery as cq
print("----", "Using", __file__, "-----")


def main(prog_rep=None):
    """
    The main structure which creates the design
    :param args: The arguments to the design in the form of a list
    :return:
    """

    prog_rep.change_mode(mode="2D", arg_nums=["0"])
    base = cq.Workplane("XY").circle(prog_rep.args[0]/2)

    prog_rep.change_mode(mode="EXTRUDE", arg_nums=["1"], prev=base)
    base = base.extrude(prog_rep.args[1])

    prog_rep.change_mode(mode="HOLE", arg_nums=["2"], prev=base)
    base = base.faces(">Z").hole(prog_rep.args[2])

    prog_rep.change_mode(mode="FILLET", arg_nums=["3"], prev=base)
    base = base.edges("%CIRCLE").fillet(prog_rep.args[3])

    return base