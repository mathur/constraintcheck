# Name: 20281_a29f9a18_2 Number of arguments: 5
# Taken from Fusion 360 Segmentation dataset

import cadquery as cq
print("----", "Using", __file__, "-----")


def main(prog_rep=None):
    """
    The main structure which creates the design
    :param args: The arguments to the design in the form of a list
    :return:
    """

    prog_rep.change_mode(mode="CREATE", arg_nums=["0", "1", "2"])
    base = cq.Workplane("XY").box(prog_rep.args[0], prog_rep.args[1], prog_rep.args[2])

    prog_rep.change_mode(mode="FILLET", arg_nums=["3"], prev=base)
    base = base.edges("|Y").fillet(prog_rep.args[3])

    prog_rep.change_mode(mode="FILLET", arg_nums=["4"], prev=base)
    base = base.edges(">Y").fillet(prog_rep.args[4])

    return base