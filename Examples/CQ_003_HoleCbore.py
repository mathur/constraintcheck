# Name: CQ_003_HoleCbore Number of arguments: 8
import cadquery as cq

print("----", "Using", __file__, "-----")


def main(prog_rep=None):
    """
    The main structure which creates the design
    :param args: The arguments to the design in the form of a list
    :return:
    """

    prog_rep.change_mode(mode="CREATE", arg_nums=["0", "1", "2"])
    box = cq.Workplane("XY").box(prog_rep.args[0], prog_rep.args[1], prog_rep.args[2])

    prog_rep.change_mode(mode="HOLE", arg_nums=["3"], prev=box)
    box = box.faces(">Z").workplane().hole(prog_rep.args[3])

    prog_rep.change_mode(mode="2D", arg_nums=["7"], prev=box)
    result = box.faces(">Z").workplane().rect(prog_rep.args[0] - prog_rep.args[7], prog_rep.args[1] - prog_rep.args[7], forConstruction=True)

    prog_rep.change_mode(mode="CBORE", arg_nums=["4", "5", "6"], prev=result)
    result = result.vertices().cboreHole(prog_rep.args[4], prog_rep.args[5], prog_rep.args[6])

    # Displays the result of this script
    # Export STL
    #f = open('Exports\\ExampleCQ003.stl', 'w')
    #cq.exporters.exportShape(shape=result, fileLike=f, exportType="STL", tolerance=0.01)
    #f.close()
    return result

