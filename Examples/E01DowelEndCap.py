# Name: E01DowelEndCap Num args: 7
# Works with correct constraints in 80 seconds

import cadquery as cq

print("----", "Using", __file__, "-----")


def main(prog_rep=None):
    """
    The main structure which creates the design
    :param args: The arguments to the design in the form of a list
    :return:
    """

    prog_rep.change_mode(mode="CREATE", arg_nums=["0", "1", "2"])
    box = cq.Workplane("XY").box(prog_rep.args[0], prog_rep.args[1], prog_rep.args[2])

    prog_rep.change_mode(mode="FILLET", arg_nums=["3"], prev=box)
    result = box.edges("|Z").fillet(prog_rep.args[3])

    prog_rep.change_mode(mode="FILLET", arg_nums=["4"], prev=result)
    result = result.edges("#Z").fillet(prog_rep.args[4])

    prog_rep.change_mode(mode="2D", arg_nums=["5"], prev=result)
    circ = result.faces(">Z").circle(prog_rep.args[5])

    prog_rep.change_mode(mode="EXTRUDE", arg_nums=["6"], prev=circ)
    result = circ.extrude(prog_rep.args[6])

    return result

