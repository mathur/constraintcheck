# Number of args: 5
# Performs a fillet on top edges, and chamfer on bottom edges.
import cadquery as cq

print("----", "Using", __file__, "-----")


def main(prog_rep=None):
    """
    The main structure which creates the design
    :param args: The arguments to the design in the form of a list
    :return:
    """
    prog_rep.change_mode(mode="CREATE", arg_nums=["0", "1", "2"])
    box = cq.Workplane("XY").box(prog_rep.args[0], prog_rep.args[1], prog_rep.args[2])

    prog_rep.change_mode(mode="FILLET", arg_nums=["3"], prev=box)
    box = box.edges(">Z").fillet(prog_rep.args[3])

    prog_rep.change_mode(mode="CHAMFER", arg_nums=["4"], prev=box)
    box = box.edges("<Z").chamfer(prog_rep.args[4])
    return box

