# Hole_Multi_Test
# A test based on the multiple holes using a for construction rectangle example.
import cadquery as cq
from OCC.Core.TopExp import TopExp_Explorer
from OCC.Core.TopAbs import *

args = [20, 25, 25, 15]

base = cq.Workplane("front").circle(args[0])

base = base.rect(args[1], args[2])

result = base.extrude(args[3])

print("Type of intermediate object:", type(base.objects[0].wrapped))
# Base objects in the list
print("Number of objects:", len(base.objects))
wire_explorer = TopExp_Explorer(base.objects[0].wrapped, TopAbs_WIRE)
wire_count = 0
while wire_explorer.More():
    wire_count = wire_count + 1
    wire_explorer.Next()
print("Number of wires:", wire_count)

# Export STL
f = open('Test.stl', 'w')
cq.exporters.exportShape(shape=result, fileLike=f, exportType="STL", tolerance=0.1)
f.close()