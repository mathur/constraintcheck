"""
Used exclusively for testing specific samples.
"""

import cadquery as cq
import OCC.Core.ShapeExtend

from OCC.Core.TopExp import TopExp_Explorer
from OCC.Core.TopAbs import TopAbs_SOLID

args = [150, 250, 40, 10, 48.14354823171404, 95.49343443604799, 42.70434832840708]

# These can be modified rather than hardcoding values for each dimension.
length = args[0]                   # Length of the block, Default: 80.0
height = args[1]                   # Height of the block, Default: 60.0
thickness = args[2]                # Thickness of the block, Default: 10.0
center_hole_dia = args[3]          # Diameter of center hole in block, Default: 22.0
cbore_hole_diameter = args[4]       # Bolt shank/threads clearance hole diameter, Default: 2.4
cbore_diameter = args[5]            # Bolt head pocket hole diameter, Default: 4.4
cbore_depth = args[6]               # Bolt head pocket hole depth, Default: 2.1

box = cq.Workplane("XY").box(length, height, thickness)
result = box.faces(">Z").workplane().hole(center_hole_dia)

#prog_rep.change_mode(mode="CBORE", arg_nums=["4", "5", "6"], prev=box)
#result = box.faces(">Z").workplane() \
#    .rect(length - 8.0, height - 8.0, forConstruction=True) \
#    .vertices().cboreHole(cbore_hole_diameter, cbore_diameter, cbore_depth)

# ShapeExtend stuff
shp = OCC.Core.ShapeExtend.ShapeExtend_Explorer()

print("Shape Type", shp.ShapeType(result.objects[0].wrapped, False))
print("Shape Type", shp.ShapeType(box.objects[0].wrapped, True))

# Count the number of solids
solid_explorer = TopExp_Explorer(result.objects[0].wrapped, TopAbs_SOLID)
solid_count = 0
while solid_explorer.More():
    solid_count = solid_count + 1
    solid_explorer.Next()
print("Found solids:", solid_count)

# Export STL
f = open('Test1.stl', 'w')
cq.exporters.exportShape(shape=result, fileLike=f, exportType="STL", tolerance=0.01)
f.close()


