# Name: CQ_033_ShellChamfer  Num of args: 5
# Example using advanced logical operators in string selectors to select only
# the inside edges on a shelled cube to chamfer.
import cadquery as cq

print("----", "Using", __file__, "-----")


def main(prog_rep=None):
    """
    The main structure which creates the design
    :param args: The arguments to the design in the form of a list
    :return:
    """
    prog_rep.change_mode(mode="CREATE", arg_nums=["0", "1", "2"])
    result = cq.Workplane("XY").box(prog_rep.args[0], prog_rep.args[1], prog_rep.args[2])

    prog_rep.change_mode(mode="SHELL", arg_nums=["3"], prev=result)
    result = result.faces(">Z").shell(-prog_rep.args[3])

    prog_rep.change_mode(mode="CHAMFER", arg_nums=["4"], prev=result)
    result = result.faces(">Z").edges("not(<X or >X or <Y or >Y)").chamfer(prog_rep.args[4])

    return result
