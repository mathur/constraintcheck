# Name: CQ_004_Extrude Num args: 4
import cadquery as cq

print("----", "Using", __file__, "-----")


def main(prog_rep=None):
    """
    The main structure which creates the design
    :param args: The arguments to the design in the form of a list
    :return:
    """

    prog_rep.change_mode(mode="2D", arg_nums=["0"])
    base = cq.Workplane("front").circle(prog_rep.args[0])

    prog_rep.change_mode(mode="2D_CUT", arg_nums=["1", "2"], prev=base)
    base = base.rect(prog_rep.args[1], prog_rep.args[2])

    prog_rep.change_mode(mode="EXTRUDE", arg_nums=["3"], prev=base)
    result = base.extrude(prog_rep.args[3])

    # Displays the result of this script
    # Export STL
    #f = open('Exports\\ExampleCQ003.stl', 'w')
    #cq.exporters.exportShape(shape=result, fileLike=f, exportType="STL", tolerance=0.01)
    #f.close()
    return result

