# We need to identify the vertex well
import cadquery as cq
from OCC.Core.BRepClass import BRepClass_FClassifier, BRepClass_FaceExplorer
from OCC.Core.gp import gp_Pnt2d, gp_Pnt
from OCC.Core.TopAbs import *
from OCC.Core.BRep import BRep_Tool
from OCC.Core.ShapeAnalysis import ShapeAnalysis_Surface
from OCC.Core.TopoDS import *
# Already included
from OCC.Core.TopExp import TopExp_Explorer

args = [150, 150, 100, 20, 30, 100, 20, 20]

base = cq.Workplane("XY").box(args[0], args[1], args[2])

base = base.edges("|Z").fillet(args[3])

base = base.faces(">Z").hole(args[4])

# Two more holes equidistant from the centre
base2 = base.edges(">Y and >Z").workplane().transformed(offset=(0,-args[5],0))
vec = base2.objects[0].wrapped
print("Using Vector with values", vec.X(), vec.Y(), vec.Z())
# Create a Pnt with values from vec

pnt = gp_Pnt()
pnt.SetXYZ(vec.XYZ())

# Go over all the faces in the intermediary object base.objects[0].wrapped
aFaceExplorer = TopExp_Explorer(base.objects[0].wrapped, TopAbs_FACE)
while aFaceExplorer.More():
    aFace = topods.Face(aFaceExplorer.Current())
    srf = BRep_Tool().Surface(aFace)
    sas = ShapeAnalysis_Surface(srf)
    face_e = BRepClass_FaceExplorer(aFace)
    classifier = BRepClass_FClassifier()
    p = sas.ValueOfUV(pnt, 0.01)
    classifier.Perform(face_e, p, 0.01)
    if classifier.State() == 0:
        print("Point on the face!")
    aFaceExplorer.Next()


base2 = base2.hole(args[6])
base2 = base2.edges("<Y and >Z").workplane().transformed(offset=(0,args[5],0)).hole(args[6])

# Export STL
f = open('Test.stl', 'w')
cq.exporters.exportShape(shape=base2, fileLike=f, exportType="STL", tolerance=0.1)
f.close()
