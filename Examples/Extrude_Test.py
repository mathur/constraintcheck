# Number of args: 4
# Original argument values: [50, 13, 19, 13]

import cadquery as cq

args = [30.80328034872792, 52.37020873142603, 59.91297270941783, 1.5248606999331038]

base = cq.Workplane("front").circle(args[0])
base = base.rect(args[1], args[2])
result = base.extrude(args[3])

# Export STL
f = open('Extrude_01.stl', 'w')
cq.exporters.exportShape(shape=result, fileLike=f, exportType="STL", tolerance=0.01)
f.close()


