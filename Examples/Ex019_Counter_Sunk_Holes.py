# Loc: Examples.Ex019_Counter_Sunk_Holes, Num of args: 8
import cadquery as cq

print("----", "Using", __file__, "-----")

# Create a plate with 4 counter-sunk holes in it.
# 1.  Establishes a workplane using an XY object instead of a named plane.
# 2.  Creates a plain box to base future geometry on with the box() function.
# 3.  Selects the top-most face of the box and established a workplane on that.
# 4.  Draws a for-construction rectangle on the workplane which only exists for
#     placing other geometry.
# 5.  Selects the corner vertices of the rectangle and places a counter-sink
#     hole, using each vertex as the center of a hole using the cskHole()
#     function.
# 5a. When the depth of the counter-sink hole is set to None, the hole will be
#     cut through.

def main(prog_rep=None):
    """
    The main structure which creates the design
    :param args: The arguments to the design in the form of a list
    :return:
    """

    prog_rep.change_mode(mode="CREATE", arg_nums=["0", "1", "2"])
    result = cq.Workplane(cq.Plane.XY()).box(prog_rep.args[0], prog_rep.args[1], prog_rep.args[2])

    prog_rep.change_mode(mode="OFFSET", arg_nums=["3", "4"], prev=result)
    result = result.faces(">Z").workplane().rect(prog_rep.args[3], prog_rep.args[4], forConstruction=True)

    prog_rep.change_mode(mode="CSKHOLE", arg_nums=["5", "6", "7"], prev=result)
    result = result.vertices().cskHole(prog_rep.args[5], prog_rep.args[6], prog_rep.args[7])

    return result