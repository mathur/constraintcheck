"""
This script includes structures to find ranges for specific CAD operations
"""
import cadquery as cq
from cadquery import Vector, Plane, Shape, Edge, Wire, Face, Solid, Compound, \
    sortWiresByBuildOrder, selectors, exporters
import random
inf = 999


def range_fillet(edges, iter=25):
    """
    Find ranges of the fillet operation param.
    Not that the param is 1-D, i.e., the radius of the fillet
    :param :shape The overall shape on which the operation is applied
    :param :edges Edges in the shape on which the operation is applied
    :param :param the param of the fillet(radius)
    :param :iter Number of ilterations which are checked
    :return [min, max]
    """
    # Set initial min, max
    min = 0
    # Set max to a realistic upper bound
    max = edges.largestDimension()
    print("-------Analysing fillet----------------")
    # Do iter iterations to find the appropriate range
    left = min
    right = max
    for iter in range(iter):
        param_temp = random.uniform(left, right)
        print("Iteration:", iter, "param:", param_temp)
        try:
            shape_temp = edges.fillet(param_temp)
        except RuntimeError:
            # Set the new right to this value
            print("Setting right to", param_temp)
            right = param_temp
            continue
        # Note: A smarter method of finding ranges is not so successful due to improper fillets being performed
    min =0
    max = right
    print("Min, max is", [min, max])
    return [min, max]


def range_chamfer(edges, iter=25):
    """
    Note: Same as the fillet operation
    Find ranges of the chamfer operation param.
    Not that the param is 1-D, i.e., the radius of the chamfer
    :param :shape The overall shape on which the operation is applied
    :param :edges Edges in the shape on which the operation is applied
    :param :param the param of the chamfer(radius)
    :param :iter Number of ilterations which are checked
    :return [min, max]
    """
    # Set initial min, max
    min = 0
    # Set max to a realistic upper bound
    max = edges.largestDimension()
    print("-------Analysing fillet----------------")
    # Do iter iterations to find the appropriate range
    left = min
    right = max
    for iter in range(iter):
        param_temp = random.uniform(left, right)
        print("Iteration:", iter, "param:", param_temp)
        try:
            shape_temp = edges.chamfer(param_temp)
        except RuntimeError:
            # Set the new right to this value
            print("Setting right to", param_temp)
            right = param_temp
            continue
    min =0
    max = right
    print("Min, max is", [min, max])
    return [min, max]


def range_hole(hole_contexts, obj, iter=25):
    """
    TODO
    NOTE: We use objects[0] to
    :param hole_contexts: The hole_contexts at which the holes shall be cut
    :param obj: The object which shall be cut
    :param iter:
    :return:
    """
    # The logic is using bounding boxes to ensure that the hole does not fully enclose the shape being cut
    # The bore hole operation has 2 degrees of freedom, diameter and depth of the hole

    d_min = 0
    d_max = obj.largestDimension()
    h_min = 0
    h_max = obj.largestDimension()

    obj_temp = cq.CQ(obj)
    for iter in range(iter):
        d_rand = random.uniform(d_min, d_max)
        # Make the cutting cylinder
        temp = hole_contexts.hole(diameter=d_rand)
        print(d_rand)
        try:
            bb_temp = temp.objects[0].BoundingBox()
        except RuntimeError:
            d_max = d_rand
            print("Changing d_max to", d_rand)
    print("Min, max is", [d_min, d_max])
    return [d_min, d_max]

