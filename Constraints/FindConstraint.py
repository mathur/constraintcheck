"""
TODO: Remove or make this script functional. Currently incomplete.
This script includes structures to check for finding implicit constraints in CAD operations.
"""
import inspect
import OCC.Core.TopExp
import OCC.Core.TopTools
# For checking planar property of the face
import OCC.Core.BRepAdaptor
# For getting parameters of the face
import OCC.Core.GProp
import OCC.Core.BRepGProp
def fillet_constraints(shape, edges, radius):
    """
    Finds the constraints while performing the fillet operation.
    It takes the same arguments as the cadquery fillet command.
    :param shape: the shape of which the edges are a part TODO: Check for edge containment in the shape not performed
    :param edges: list of edges that need to be filleted
    :param radius: radius of the fillet
    :return: TODO
    """
    print("Checking for constraints on fillet")
    # Get the OCC shape
    shape = shape.objects[0].wrapped
    # Get TopoDS_Edge objects
    edge_list = [e.wrapped for e in edges.edges().vals()]
    # Map edges-->faces for the whole shape
    edge_face_map = OCC.Core.TopTools.TopTools_IndexedDataMapOfShapeListOfShape()
    OCC.Core.TopExp.topexp_MapShapesAndAncestors(shape,
                                 OCC.Core.TopAbs.TopAbs_EDGE,
                                 OCC.Core.TopAbs.TopAbs_FACE,
                                 edge_face_map)
    for e in edge_list:
        # We assume there are just two faces
        face1 = edge_face_map.FindFromKey(e).First()
        face2 = edge_face_map.FindFromKey(e).Last()
        print(face1, face2)
        # Check that the faces are planar
        # Do some specific checks on area depending on the type of face.
        #gprop = OCC.Core.GProp.GProp_GProps()
        #brep_gprop = OCC.Core.BRepGProp.brepgroup.SurfaceProperties(S=face1, SProps=gprop)
        #print(gprop.Mass())
