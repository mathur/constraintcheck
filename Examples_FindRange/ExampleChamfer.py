# Inspired by https://www.thingiverse.com/thing:3814727

import cadquery as cq
import Constraints.FindRange as find_range

dim = 10
chamfer_rad = 2
print("SimpleExample")

obj = cq.Workplane("XY").box(dim, dim, dim)
find_range.range_chamfer(edges=obj.edges(">Z"))
obj = obj.edges(">Z").chamfer(chamfer_rad)


# Export STL
f = open('Exports\\ExampleChamfer.stl', 'w')
cq.exporters.exportShape(shape=obj, fileLike=f, exportType="STL", tolerance=0.01)
f.close()

# obj = cq.importShape("xxx")
# # bb = obj.BoundingBox()
# # bb.x_min