# Inspired by https://www.thingiverse.com/thing:3814727

import cadquery as cq
import Constraints.FindRange as find_range

dim = 10
hole_dia = 10
hole_depth = 12
print("SimpleExample")

obj = cq.Workplane("XY").box(dim, dim, dim)
find_range.range_hole(hole_contexts=obj.faces(">Z"), obj=obj)
obj = obj.faces(">Z").hole(diameter=hole_dia, depth=hole_depth)


# Export STL
f = open('Exports\\ExampleHole.stl', 'w')
cq.exporters.exportShape(shape=obj, fileLike=f, exportType="STL", tolerance=0.01)
f.close()
