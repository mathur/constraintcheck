# A test for finding odd regions passing / failing for a simple fillet
# Stricter tests to ensure that

import cadquery as cq
import random
import numpy as np
# Counting number of solids using Python OCC
from OCC.Core.TopExp import TopExp_Explorer
from OCC.Core.TopAbs import TopAbs_SOLID
from OCC.Core.BRepCheck import BRepCheck_Analyzer


# Segmentation mode: 
# Dimensions of the box
dim = 50

# Min / Max values of the fillet sample
min = 1
max = 100

num_iters = 1000
vals = []
for i in range(num_iters):
    vals.append(random.uniform(min, max))

# Create the box
obj = cq.Workplane("XY").box(dim, dim, dim)

passing = np.array([])
failing = np.array([])
for val in vals:
    try:
        res = obj.edges(">Z").fillet(val)

        # Additional checks on the result
        obj.objects[0].BoundingBox()

        # B-rep validity check
        validity_brep = BRepCheck_Analyzer(obj.objects[0].wrapped)
        assert validity_brep.IsValid()
        # Get the number of solids in the obj
        solid_explorer = TopExp_Explorer(obj.objects[0].wrapped, TopAbs_SOLID)
        solid_count = 0
        while solid_explorer.More():
            solid_count = solid_count + 1
            solid_explorer.Next()
        # Assert that there is 1 solid
        if solid_count > 0:
            assert (solid_count == 1)

        passing = np.append(passing, val)
    except (RuntimeError, IndexError, AttributeError, AssertionError, ValueError, TypeError) as e:
        print("Failing due to ", e)
        failing = np.append(failing, val)
        continue

print("Passing vals:", np.sort(passing))
print("Min passing:", np.amin(passing), "Max passing:", np.amax(passing))
print("Failing vals:", np.sort(failing))
print("Min failing:", np.amin(failing), "Max failing:", np.amax(failing))
