# A test for finding odd regions passing / failing for a simple fillet

import cadquery as cq
import random
import numpy as np

# Segmentation mode:
# Dimensions of the box
dim = 50

# Min / Max values of the fillet sample
min = 1
max = 100

num_iters = 1000
vals = []
for i in range(num_iters):
    vals.append(random.uniform(min, max))

# Create the box
obj = cq.Workplane("XY").box(dim, dim, dim)

passing = np.array([])
failing = np.array([])
for val in vals:
    try:
        res = obj.edges(">Z").fillet(val)
        passing = np.append(passing, val)
    except RuntimeError:
        failing = np.append(failing, val)
        continue

print("Passing vals:", np.sort(passing))
print("Min passing:", np.amin(passing), "Max passing:", np.amax(passing))
print("Failing vals:", np.sort(failing))
print("Min failing:", np.amin(failing), "Max failing:", np.amax(failing))
