# Inspired by https://www.thingiverse.com/thing:3814727

import cadquery as cq
import Constraints.FindRange as find_range

dim = 10
fillet_rad = 2
print("SimpleExample")

obj = cq.Workplane("XY").box(dim, dim, dim)

find_range.range_fillet(edges=obj.edges(">Z"))
obj = obj.edges(">Z").fillet(fillet_rad)


# Export STL
f = open('Exports\\ExampleFillet.stl', 'w')
cq.exporters.exportShape(shape=obj, fileLike=f, exportType="STL", tolerance=0.01)
f.close()