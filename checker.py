"""
This script contains interfaces to do random testing on an input program
Usage (naive): checker.py <prog_to_check> "naive" <num_of_args> <num_iters>
Usage (stateful): checker.py <prog_to_check> "stateful" <num_of_args> <num_iters>
Usage (stateful sort): checker.py <prog_to_check> "stateful_sort" <num_of_args> <num_iters>

NOTE: Currently, the sorted mode works only on programs with a single operation.
    This is because success / fail is only tracked at an overall level, not step level.
    This will be extended in the future.
"""
import sys
import inspect
import importlib
import random
import fitter
import prog_representation
import cadquery as cq
import svm
import time


def naive(prog, num_args, num_iters):
    """
    A naive implementation of the checker
    :return:
    """
    # Make a prog representation with requisite num of arguments
    rep = prog_representation.prog_rep(num_args)

    for i in range(num_iters):
        # Generate arguments to be passed
        rep.generate_vars_naive()
        try:
            result = prog.main(prog_rep=rep, args=rep.args)
            # print("Succeeded at", i)
            rep.check(result, True)
            # Successful iteration
            # print("Succeeded at", i)
        except (RuntimeError, IndexError, AttributeError, AssertionError, ValueError):
            # print("Failed at", i)
            rep.handle_exception()
        # The program's iteration is complete, now we can aggregate for this iteration
        rep.fin_it()
    # All of the iterations are complete
    rep.fin_all()
    return rep


def stateful(prog, num_args, num_iters):
    """
    Maintains state of the program for a better sampling
    :param prog:
    :param num_args:
    :param num_iters:
    :return:
    """
    # Make a prog representation with requisite num of arguments
    rep = prog_representation.prog_rep(num_args)
    for i in range(num_iters):
        # Generate arguments to be passed
        rep.generate_vars_stateful()
        try:
            result = prog.main(prog_rep=rep)
            # print("Succeeded at", i)
            rep.check(result, True)
        except (RuntimeError, IndexError, AttributeError, AssertionError, ValueError, TypeError) as e:
            # AttributeError on cut hole
            # print("Failed at", i)
            rep.handle_exception(e)
        # The program's iteration is complete, now we can aggregate for this iteration
        rep.fin_it()
    # All of the iterations are complete
    rep.fin_all()
    return rep


def stateful_sort(prog, num_args, min_samples):
    """
    Maintains state of the program for a better sampling
    :param prog:
    :param num_args:
    :param min_samples: The minimum number of samples per may constraint so that it becomes a must constraint.
    :return:
    """
    # Make a prog representation with requisite num of arguments
    rep = prog_representation.prog_rep(num_args, sort_mode=True, min_samples=min_samples)
    done = False
    while done is not True:
        # Generate arguments to be passed
        rep.generate_vars_stateful()
        try:
            result = prog.main(prog_rep=rep)
            # print("Succeeded at", i)
            rep.check(result, True)
        except (RuntimeError, IndexError, AttributeError, AssertionError, ValueError, TypeError) as e:  # AttributeError on cut hole
            # print("Failed at", i)
            rep.handle_exception(e)
        # The program's iteration is complete, now we can aggregate for this iteration
        done = rep.fin_it()
    # All of the iterations are complete
    rep.fin_all()
    return rep


def stateful_sort_single(prog, num_args, min_samples):
    """
    Maintains state of the program for a better sampling
    :param prog:
    :param num_args:
    :param min_samples: The minimum number of samples per may constraint so that it becomes a must constraint.
    :return:
    """
    # Make a prog representation with requisite num of arguments
    rep = prog_representation.prog_rep(num_args, sort_mode=True, min_samples=min_samples, onebyone=True)
    done = False
    while done is not True:
        # Generate arguments to be passed
        rep.generate_vars_stateful_singular()
        try:
            result = prog.main(prog_rep=rep)
            # print("Succeeded at", i)
            rep.check(result, True)
        except (RuntimeError, IndexError, AttributeError, AssertionError, ValueError, TypeError) as e:  # AttributeError on cut hole
            # print("Failed at", i)
            rep.handle_exception(e)
        # The program's iteration is complete, now we can aggregate for this iteration
        done = rep.fin_it()
    # All of the iterations are complete
    rep.fin_all()
    return rep


# Analyze a program wrt success % with random sampling
def analyze_random(prog, num_args):
    # Make a prog representation with requisite num of arguments
    rep = prog_representation.prog_rep(num_args, sort_mode=False, lp_mode=False, analyze_mode=0, max_samples=1000)
    done = False
    while done is not True:
        # Generate arguments to be passed
        rep.generate_vars_naive()
        try:
            result = prog.main(prog_rep=rep)
            rep.check(result, True)  # check is called only if there is success
        except (RuntimeError, IndexError, AttributeError, AssertionError, ValueError,
                TypeError) as e:  # AttributeError on cut hole
            # print("Failed at", i)
            rep.handle_exception(e)
        # The program's iteration is complete, now we can aggregate for this iteration
        done = rep.fin_it()

    # All of the iterations are complete
    rep.fin_all()
    return rep


def use_svm(prog, num_args, num_iters):
    """
    Maintains state of the program for a better sampling
    :param prog:
    :param num_args:
    :param num_iters:
    :return:
    """
    # Make a prog representation with requisite num of arguments
    rep = prog_representation.prog_rep(num_args)

    for i in range(num_iters):
        # Generate arguments to be passed
        rep.generate_vars_naive()
        try:
            result = prog.main(prog_rep=rep)
            # result is a workplane object. We take the bounding box of the first object to check if it is empty.
            result.objects[0].BoundingBox()
            # Successful iteration
            # print("Succeeded at", i)
            rep.handle_success()
        except (RuntimeError, IndexError, AttributeError, TypeError) as exc:
            # print("Failed at", i)
            rep.handle_exception(exc)
    # All of the iterations are complete
    svm.build(rep)
    return rep


def stateful_svm(prog, num_args, min_samples):
    """
    Use an SVM +  store state.
    :param prog:
    :param num_args:
    :param min_samples: The minimum number of samples per may constraint so that it becomes a must constraint.
    :return:
    """
    # Make a prog representation with requisite num of arguments
    rep = prog_representation.prog_rep(num_args, sort_mode=False, min_samples=min_samples, svm_mode=True)
    done = False
    while done is not True:
        # Generate arguments to be passed
        rep.generate_vars_stateful_svm()
        try:
            result = prog.main(prog_rep=rep)
            # print("Succeeded at", i)
            rep.check(result, True)
        except (RuntimeError, IndexError, AttributeError, AssertionError, ValueError, TypeError) as e:  # AttributeError on cut hole
            # print("Failed at", i)
            rep.handle_exception(e)
        # The program's iteration is complete, now we can aggregate for this iteration
        done = rep.fin_it()
    # All of the iterations are complete
    rep.fin_all()
    return rep


def sort_svm_hybrid(prog, num_args, min_samples):
    """
    Use stateful SVM + Sort based approach.
    :param prog:
    :param num_args:
    :param min_samples: The minimum number of samples per may constraint so that it becomes a must constraint.
    :return:
    """
    # Make a prog representation with requisite num of arguments
    rep = prog_representation.prog_rep(num_args, sort_mode=True, min_samples=min_samples, svm_mode=True)
    done = False
    while done is not True:
        # Generate arguments to be passed
        rep.generate_vars_hybrid()
        try:
            result = prog.main(prog_rep=rep)
            # print("Succeeded at", i)
            rep.check(result, True)
        except (RuntimeError, IndexError, AttributeError, AssertionError, ValueError, TypeError) as e:  # AttributeError on cut hole
            # print("Failed at", i)
            rep.handle_exception(e)
        # The program's iteration is complete, now we can aggregate for this iteration
        done = rep.fin_it()

    # All of the iterations are complete
    rep.fin_all()
    return rep


def stateful_lp(prog, num_args, min_samples, max_samples, analyze=None):
    """
    Use stateful LP via PuLP
    :param prog: the CAD program
    :param num_args: the num of args in the analysed program
    :param min_samples: The minimum number of samples per may constraint so that it becomes a must constraint.
    :return:
    """
    # Make a prog representation with requisite num of arguments
    rep = prog_representation.prog_rep(num_args, sort_mode=False, min_samples=min_samples, max_samples=max_samples, lp_mode=True)
    done = False
    while done is not True:
        # Generate arguments to be passed
        rep.generate_vars_lp()
        try:
            result = prog.main(prog_rep=rep)
            rep.check(result, True)  # check is called only if there is success
        except (RuntimeError, IndexError, AttributeError, AssertionError, ValueError, TypeError) as e:  # AttributeError on cut hole
            # print("Failed at", i)
            rep.handle_exception(e)
        # The program's iteration is complete, now we can aggregate for this iteration
        done = rep.fin_it()

    # All of the iterations are complete
    rep.fin_all()

    if analyze is not None:
        # Analyze solution
        print("Now analyzing the solution")
        rep.analyze_mode = 1
        rep.max_samples = 100
        rep.analyze_clear_counting()
        done = False
        while done is not True:
            # Generate arguments to be passed
            rep.generate_vars_lp()
            try:
                result = prog.main(prog_rep=rep)
                rep.check(result, True)  # check is called only if there is success
            except (RuntimeError, IndexError, AttributeError, AssertionError, ValueError, TypeError) as e:  # AttributeError on cut hole
                # print("Failed at", i)
                rep.handle_exception(e)
            # The program's iteration is complete, now we can aggregate for this iteration
            done = rep.fin_it()
        rep.fin_all()

    return rep


# An automated version using LP, with extra analysis of before and after success % with sampling
def automated_lp(prog, num_args, min_samples, max_samples, out_file):
    """
    Use stateful LP via PuLP
    :param prog: the CAD program
    :param num_args: the num of args in the analysed program
    :param min_samples: The minimum number of samples per may constraint so that it becomes a must constraint.
    :return:
    """
    #--------------------------------------------------------------------------
    # Get a snapshot of how naive random sampling is
    out_file.write("Random analysis")
    rep = prog_representation.prog_rep(num_args, sort_mode=False, lp_mode=False, analyze_mode=0, max_samples=1000,
                                       out_file=out_file)
    done = False
    while done is not True:
        # Generate arguments to be passed
        rep.generate_vars_naive()
        try:
            result = prog.main(prog_rep=rep)
            rep.check(result, True)  # check is called only if there is success
        except (RuntimeError, IndexError, AttributeError, AssertionError, ValueError,
                TypeError) as e:  # AttributeError on cut hole
            # print("Failed at", i)
            rep.handle_exception(e)
        # The program's iteration is complete, now we can aggregate for this iteration
        done = rep.fin_it()
    # All of the iterations are complete
    rep.fin_all()

    # --------------------------------------------------------------------------
    # Now, do LP mode
    out_file.write("\n\nFinding constraints")
    rep = prog_representation.prog_rep(num_args, sort_mode=False, min_samples=min_samples, max_samples=max_samples,
                                       lp_mode=True, out_file=out_file)
    done = False
    while done is not True:
        # Generate arguments to be passed
        rep.generate_vars_lp()
        try:
            result = prog.main(prog_rep=rep)
            rep.check(result, True)  # check is called only if there is success
        except (RuntimeError, IndexError, AttributeError, AssertionError, ValueError, TypeError) as e:  # AttributeError on cut hole
            # print("Failed at", i)
            rep.handle_exception(e)
        # The program's iteration is complete, now we can aggregate for this iteration
        done = rep.fin_it()

    # All of the iterations are complete
    rep.fin_all()

    # --------------------------------------------------------------------------
    # Analyze solution
    out_file.write("\n\nAnalyzing solution")
    rep.analyze_mode = 1
    rep.max_samples = 1000
    rep.analyze_clear_counting()
    done = False
    while done is not True:
        # Generate arguments to be passed
        rep.generate_vars_lp()
        try:
            result = prog.main(prog_rep=rep)
            rep.check(result, True)  # check is called only if there is success
        except (RuntimeError, IndexError, AttributeError, AssertionError, ValueError, TypeError) as e:  # AttributeError on cut hole
            # print("Failed at", i)
            rep.handle_exception(e)
        # The program's iteration is complete, now we can aggregate for this iteration
        done = rep.fin_it()
    rep.fin_all()

    return rep


if __name__ == "__main__":
    print("Running the checker...")
    # Get vals from command line
    prog = importlib.import_module(sys.argv[1])
    mode = sys.argv[2]
    num_args = int(sys.argv[3])
    num_iters = int(sys.argv[4])
    if mode == "naive":
        print("Naive checking...")
        rep = naive(prog=prog, num_args=num_args, num_iters=num_iters)
    elif mode == "stateful":
        print("Stateful checking...")
        rep = stateful(prog=prog, num_args=num_args, num_iters=num_iters)
    elif mode == "stateful_sort":
        print("Stateful sort-based checking...")
        rep = stateful_sort(prog=prog, num_args=num_args, min_samples=num_iters)
    elif mode == "stateful_sort_single":
        print("Stateful singular sort-based checking...")
        rep = stateful_sort_single(prog=prog, num_args=num_args, min_samples=num_iters)
    elif mode == "stateful_svm":
        print("Stateful SVM based checking...")
        rep = stateful_svm(prog=prog, num_args=num_args, min_samples=num_iters)
    elif mode == "svm":
        print("Checking via Support Vector Machine...")
        rep = use_svm(prog=prog, num_args=num_args, num_iters=num_iters)
    elif mode == "sort_svm_hybrid":
        print("Hybrid checking...")
        rep = sort_svm_hybrid(prog=prog, num_args=num_args, min_samples=num_iters)
    elif mode == "stateful_lp":
        rep = stateful_lp(prog=prog, num_args=num_args, min_samples=num_iters, max_samples=8*num_iters)
    elif mode == "analyze_random":
        rep = analyze_random(prog=prog, num_args=num_args)
    elif mode == "automated_lp":
        # Output will be put into a file, so let us create it
        f = open(sys.argv[1] + ".info", "w")
        rep = automated_lp(prog=prog, num_args=num_args, min_samples=num_iters, max_samples=8*num_iters,
                           out_file=f)
        f.close()

    print("Done!")

